using GLib;
using Gtk;
using Gdk;
using GL;
using GLU;
using Cairo;

[CCode(
   cheader_filename = "stdint.h",
   cheader_filename = "actor.h"
)]

public class GlCanvas : Gtk.DrawingArea {
	//private GL.GLuint Textures[2];
	public GLContext* gl_context = null;
	public GLDrawable* gl_drawable = null;
	public static GLConfig* glconfig;
	public Actor actor;

	private bool gl_initialised = false;
	private static int vborder = 8;

	public static GlCanvas instance;

	public GlCanvas ()
	{
		add_events (Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK | Gdk.EventMask.POINTER_MOTION_MASK);

		set_size_request (200, 100);
		set_can_focus(true);

		if(!(bool)glconfig) glconfig = new GLConfig.by_mode (GLConfigMode.RGB | GLConfigMode.DOUBLE);
		//if (!glconfig) { gerr ("Cannot initialise gtkglext."); return NULL; }                           // throw ?
		WidgetGL.set_gl_capability (this, glconfig, null, true, GLRenderType.RGBA_TYPE);

		gl_query_extension();

		instance = this;
	}

	public override void realize ()
	{
		base.realize();

		if((bool)gl_context) return;

		if(get_realized()){
			gl_drawable = WidgetGL.get_gl_drawable (this);
			gl_context = WidgetGL.get_gl_context (this);

			gl_init();

			setup_projection();
		}
	}

	public override void unrealize ()
	{
		base.unrealize();

		gl_context = null; // the context cannot be reused if the x window changes, eg when using xembed.
		gl_drawable = null;
	}

	private void gl_init()
	{
		if(gl_initialised) return;

		START_DRAW(); {

			if(!AGl.shaders_supported()){
				warn("shaders not supported");
				print("Warning: this program prefers OpenGL 2.0\n");
			}

			if((bool)_debug_){
				//print("GL_RENDERER = %s\n", (string)glGetString(GL_RENDERER));
				//print("OpenGL version  : %s\n", (string)glGetString(GL_VERSION));
				//print("OpenGL vendor   : %s\n", (string)glGetString(GL_VENDOR));
				//print("OpenGL renderer : %s\n", (string)glGetString(GL_RENDERER));
				//printf("GL_MAX_TEXTURE_IMAGE_UNITS        : %d\n", maxTexUnits);

				GLint maxTexSize = 0; // maximum allowed size for our 2D textures
				glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTexSize);
				print("GL_MAX_TEXTURE_SIZE               : %d\n", (int)maxTexSize);

				GLint texSize = 0;
				glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE_ARB, &texSize);
				print("GL_MAX_RECTANGLE_TEXTURE_SIZE_ARB : %d\n", (int)texSize);
			}

		} END_DRAW();

		gl_initialised = true;
	}

	private void setup_projection()
	{
		int vx = 0;
		int vy = 0;
		int vw = allocation.width;
		int vh = allocation.height;
		glViewport(vx, vy, vw, vh);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		// establish clipping volume (left, right, bottom, top, near, far)
		double left = 0.0;
		double right = vw;
		double top   = vh;
		double bottom = 0.0;
		glOrtho (left, right, bottom, top, 10.0, -100.0);

		actor.set_size(vw, vh - 2 * vborder);
	}

#if 0
	public override bool button_press_event (Gdk.EventButton event) {
		return false;
	}

	public override bool button_release_event (Gdk.EventButton event) {
		return false;
	}

	public override bool motion_notify_event (Gdk.EventMotion event) {
		return false;
	}
#endif

	public override void size_allocate (Gdk.Rectangle allocation)
	{
		base.size_allocate(allocation);

		if(!gl_initialised) return;

		setup_projection();
	}

	public override bool expose_event (Gdk.EventExpose event)
	{

		if(!get_realized()) return true;
		if(!gl_initialised) return true;

		START_DRAW(); {
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			actor.draw((Gtk.Widget*)this);

			gl_drawable->swap_buffers ();
		} END_DRAW();

		return true;
	}

	public void print_config ()
	{
		print("\nOpenGL configuration:\n\n");

		print("is_rgba (glconfig) = %s\n",            glconfig->is_rgba() ? "TRUE" : "FALSE");
		print("is_double_buffered (glconfig) = %s\n", glconfig->is_double_buffered() ? "TRUE" : "FALSE");
		print("is_stereo (glconfig) = %s\n",          glconfig->is_stereo () ? "TRUE" : "FALSE");
		print("has_alpha (glconfig) = %s\n",          glconfig->has_alpha () ? "TRUE" : "FALSE");
		print("has_depth_buffer (glconfig) = %s\n",   glconfig->has_depth_buffer () ? "TRUE" : "FALSE");
		print("has_stencil_buffer (glconfig) = %s\n", glconfig->has_stencil_buffer () ? "TRUE" : "FALSE");
		print("has_accum_buffer (glconfig) = %s\n",   glconfig->has_accum_buffer () ? "TRUE" : "FALSE");
		print("\n");

		print_config_attrib ("GDK_GL_USE_GL",           GLConfigAttrib.USE_GL,           true);
		print_config_attrib ("GDK_GL_BUFFER_SIZE",      GLConfigAttrib.BUFFER_SIZE,      false);
		print_config_attrib ("GDK_GL_LEVEL",            GLConfigAttrib.LEVEL,            false);
		print_config_attrib ("GDK_GL_RGBA",             GLConfigAttrib.RGBA,             true);
		print_config_attrib ("GDK_GL_DOUBLEBUFFER",     GLConfigAttrib.DOUBLEBUFFER,     true);
		print_config_attrib ("GDK_GL_STEREO",           GLConfigAttrib.STEREO,           true);
		print_config_attrib ("GDK_GL_AUX_BUFFERS",      GLConfigAttrib.AUX_BUFFERS,      false);
		print_config_attrib ("GDK_GL_RED_SIZE",         GLConfigAttrib.RED_SIZE,         false);
		print_config_attrib ("GDK_GL_GREEN_SIZE",       GLConfigAttrib.GREEN_SIZE,       false);
		print_config_attrib ("GDK_GL_BLUE_SIZE",        GLConfigAttrib.BLUE_SIZE,        false);
		print_config_attrib ("GDK_GL_ALPHA_SIZE",       GLConfigAttrib.ALPHA_SIZE,       false);
		print_config_attrib ("GDK_GL_DEPTH_SIZE",       GLConfigAttrib.DEPTH_SIZE,       false);
		print_config_attrib ("GDK_GL_STENCIL_SIZE",     GLConfigAttrib.STENCIL_SIZE,     false);
		print_config_attrib ("GDK_GL_ACCUM_RED_SIZE",   GLConfigAttrib.ACCUM_RED_SIZE,   false);
		print_config_attrib ("GDK_GL_ACCUM_GREEN_SIZE", GLConfigAttrib.ACCUM_GREEN_SIZE, false);
		print_config_attrib ("GDK_GL_ACCUM_BLUE_SIZE",  GLConfigAttrib.ACCUM_BLUE_SIZE,  false);
		print_config_attrib ("GDK_GL_ACCUM_ALPHA_SIZE", GLConfigAttrib.ACCUM_ALPHA_SIZE, false);

		print("\n");
	}

	void print_config_attrib (char* attrib_str, int attrib, bool is_boolean)
	{
		int value;

		print("%s = ", (string)attrib_str);
		if(glconfig->get_attrib(attrib, out value)){
			if (is_boolean) print("%s\n", (bool)value ? "TRUE" : "FALSE");
			else print("%d\n", value);
		}
		else print("*** Cannot get %s attribute value\n", (string)attrib_str);
	}
}

