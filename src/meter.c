/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __meter_c__
#include "config.h"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <gdk/gdkkeysyms.h>
#include "agl/ext.h"
#include "agl/text/pango.h"
#include "ayyi/services/ardourd.h"
#include "model/ayyi_model.h"
#include "wf/promise.h"

#include "convolution.h"
#include "post.h"
#define USE_CANVAS
#ifdef USE_CANVAS
#  include "gl_canvas.h"
#else
#  include "glarea.h"
#endif
#include "nodes/meter.h"
#include "meter.h"

static void     meter_ready              (gpointer, gpointer);
static void     meter_on_connect         (GError*, gpointer);
static void     meter_connect_signals    ();
static void     meter_on_canvas_realise  (GtkWidget*, gpointer);
static void     ayyi_meter_on_unrealise  (GtkWidget*, gpointer);
static void     meter_on_track_add       (GObject*, AMTrack*, gpointer);
static void     meter_on_track_delete    (GObject*, GList*, gpointer);
static void     observer__transport_play ();
static void     rebuild_tracks           ();
static gboolean _ayyi_meter_periodic_update(gpointer);
#if 0
static gboolean on_destroy               (GtkWidget*, GdkEvent*, gpointer);
#endif
static void     on_unmap                 (GtkWidget*, gpointer);
#ifdef NOT_USED
static void     print_widget_tree        (GtkWidget*);
#endif

guint           timer;
#define DIRECT 1
#undef SMALL_KERNEL
#define unref0(A) (g_object_unref(A), A = NULL)

#ifdef SMALL_KERNEL
	GLint kernel_size = 9;
#else
	//GLint kernel_size = 49;
	GLint kernel_size = 25;
#endif

MeterActor* meter_actor = NULL;

GtkWidget*     canvas = NULL;
static void    (*on_canvas_ready)() = NULL;

gboolean __drawing = FALSE;
#include "shader.c"

enum {PROMISE_REALISED=0, PROMISE_CONNECTED, PROMISE_NULL, PROMISE_MAX};

struct Promises {
	AMPromise* m;
	AMPromise* p[PROMISE_MAX];
} promises;


GtkWidget*
ayyi_meter_new (int argc, char** argv, void (*_on_canvas_ready)())
{
	on_canvas_ready = _on_canvas_ready;

	{
		// create a promise that will resolve once the song is connected
		// and the canvas is realised.
		promises = (struct Promises){
			am_promise_new(NULL),
			{am_promise_new(NULL), am_promise_new(NULL), NULL}
		};
		am_promise_when(promises.m, promises.p[0], promises.p[1], NULL);
		am_promise_add_callback(promises.m, meter_ready, NULL);
	}

	if(!ayyi.service){
#ifdef USE_SONG_SERVICE
		am__init(NULL); //use default services
#else
		AyyiService* services[] = {
			(AyyiService*)&ardourd_mixer,
			NULL,
			NULL,
		};
		am__init((AyyiService**)&services);
#endif
		ayyi.log.to_stdout = false;

#ifdef USE_SONG_SERVICE
		am__connect_services(ayyi.services, meter_on_connect, NULL);
#else
		am__connect_services((AyyiService**)&services, meter_on_connect, NULL);
#endif
	}else{
		am_promise_resolve(promises.p[PROMISE_CONNECTED], &(PromiseVal){0});
	}

	gtk_gl_init(&argc, &argv);

	gboolean key_press (GtkWidget* widget, GdkEventKey* event, gpointer user_data)
	{
		switch(event->keyval){
			case GDK_KP_Enter:
				ayyi_transport_play();
				return AGL_HANDLED;
			case GDK_space:
			case 0xff9e:
				ayyi_transport_stop();
				return AGL_HANDLED;
			case GDK_Delete:
				canvas_destroy();
				return AGL_HANDLED;
			default:
				dbg(1, "%i", event->keyval);
				break;
		}
		return AGL_NOT_HANDLED;
	}

	canvas_new();

#if 0
	if(false) gl_canvas_print_config ((GlCanvas*)canvas);
#endif

	if(gtk_widget_get_toplevel(canvas)){
		g_signal_connect(canvas, "key-press-event", G_CALLBACK(key_press), NULL);
		gtk_widget_grab_focus(canvas);
	}

	gtk_widget_show(canvas);
	return canvas;
}


static void
meter_connect_signals ()
{
	g_signal_connect(song->tracks, "add", G_CALLBACK(meter_on_track_add), NULL);
	g_signal_connect(song->tracks, "delete", G_CALLBACK(meter_on_track_delete), NULL);
	am_song__connect("transport-play", G_CALLBACK(observer__transport_play), NULL);

	am_song__enable_meters(true);
}


static void
meter_on_connect (GError* e, gpointer user_data)
{
	dbg(1, "got_shm=%i", ayyi.got_shm);
	if (!ayyi.got_shm) exit(EXIT_FAILURE);

	PromiseVal val = {.i = 8};
	am_promise_resolve(promises.p[PROMISE_CONNECTED], &val);
}


static void
meter_ready (gpointer _, gpointer __)
{
	PF;

#ifdef USE_CANVAS
	AGlActor* actor0 = ((GlCanvas*)canvas)->actor;
	shaders_init();
#else
	AGlActor* actor0 = (AGlActor*)((GlArea*)canvas)->scene;
#endif

	extern AGlActor* labels_y_new();
	agl_actor__add_child(actor0, labels_y_new());

	{

#ifndef DISABLE_METER_NODE
		meter_actor = (MeterActor*)agl_actor__add_child(actor0, meter_actor_new());
		meter_actor->tracklist = observable_new();

		agl_actor__set_size((AGlActor*)meter_actor);
#endif

	#ifdef USE_SONG_SERVICE
		extern AGlActor* time_actor_new();
		if(!GTK_IS_PLUG(gtk_widget_get_toplevel(((AGlActor*)meter_actor)->root->gl.gdk.widget))){
			agl_actor__add_child(actor0, time_actor_new());
		}
	#endif
	#if 0
		extern Actor* test_actor_new();
		actor_add_child(actor0, test_actor_new());
	#endif

		if(on_canvas_ready) on_canvas_ready();

		agl_actor__invalidate(actor0);
	}

	rebuild_tracks();

	meter_connect_signals();

	extern AGlActor* labels_new();
	agl_actor__add_child(actor0, labels_new());
}


GtkWidget*
canvas_new ()
{
	PF;

#ifdef USE_CANVAS
	canvas = (GtkWidget*)gl_canvas_new();
#else
	canvas = (GtkWidget*)gl_area_new();
	((AGlActor*)((GlArea*)canvas)->scene)->init = shaders_init;
#endif

	gtk_widget_set_size_request(GTK_WIDGET(canvas), 256, 128);

	g_signal_connect((gpointer)canvas, "realize", G_CALLBACK(meter_on_canvas_realise), NULL);
	g_signal_connect((gpointer)canvas, "unrealize", G_CALLBACK(ayyi_meter_on_unrealise), NULL);

#if 0
	//not being called
	g_signal_connect(G_OBJECT(canvas), "destroy-event", G_CALLBACK(on_destroy), NULL);

	//unmap not being called
	gtk_widget_add_events(canvas, GDK_STRUCTURE_MASK);
#endif
	g_signal_connect(G_OBJECT(canvas), "unmap", G_CALLBACK(on_unmap), NULL);

	void canvas_finalize_notify (gpointer data, GObject* was)
	{
		PF;
		canvas = NULL;
	}
	g_object_weak_ref((GObject*)canvas, canvas_finalize_notify, NULL);

	return canvas;
}


void
canvas_destroy ()
{
	PF;

#if 0
	bool on_after_destroy(gpointer data)
	{
		dbg(0, "window is widget? %i", GTK_IS_WIDGET(window));

		canvas_new();
		gtk_widget_show(canvas);
		gtk_container_add(GTK_CONTAINER(window), canvas);

		return TIMER_STOP;
	}
#endif

	//unref0(canvas);

	GtkWidget* c = canvas;
	canvas = NULL;
	gtk_widget_destroy(c);

	//g_timeout_add(4000, on_after_destroy, NULL);
}


#if 0
static gboolean
on_destroy (GtkWidget* widget, GdkEvent* event, gpointer user_data)
{
	PF;
	return TRUE;
}
#endif


static void
on_unmap (GtkWidget* widget, gpointer user_data)
{
	PF;
}


static void
meter_on_canvas_realise (GtkWidget* canvas, gpointer user_data)
{
	PF;
	if(!GTK_WIDGET_REALIZED (canvas)) return;

	PromiseVal val = {.i = 7};
	am_promise_resolve(promises.p[PROMISE_REALISED], &val);

	if(!canvas) canvas_new(); // for after a reparent
}


static void
ayyi_meter_on_unrealise (GtkWidget* widget, gpointer user_data)
{
	PF;
	if(canvas){
		g_object_unref(canvas);
		canvas = NULL;
	}

	am_promise_unref(promises.p[PROMISE_REALISED]);
	am_promise_unref(promises.m);

	promises.m = am_promise_new(NULL);
	promises.p[PROMISE_REALISED] = am_promise_new(NULL);
	am_promise_when(promises.m, promises.p[PROMISE_REALISED], promises.p[PROMISE_CONNECTED], NULL);
	am_promise_add_callback(promises.m, meter_ready, NULL);


#if 0
	dbg(2, "embedded=%i", xembed_plug_get_embedded(XEMBED_PLUG(window)));
	dbg(2, "socket=%p", xembed_plug_get_socket_window (XEMBED_PLUG(window)));
#endif

#if 0
	{
		bool on_after_unrealise(gpointer data)
		{
			dbg(0, "...");

			window = xembed_plug_new_for_display(gdk_display_get_default(), socket_id);
			if(canvas->parent){
				gtk_widget_reparent(canvas, window);
			}else{
				gtk_container_add(GTK_CONTAINER(window), canvas);
			}
			gtk_widget_show_all(window);
			dbg(0, "embedded=%i", xembed_plug_get_embedded(XEMBED_PLUG(window)));
			dbg(0, "id=%x", xembed_plug_get_id(XEMBED_PLUG(window)));

 			return TIMER_STOP;
		}

		g_timeout_add(2000, on_after_unrealise, NULL);
	}
#endif
}


/*
 *  TODO only update channels that are active. Requires each channel to be rendered as a separate node
 */
static gboolean
_ayyi_meter_periodic_update (gpointer data)
{
	if (!ayyi.got_shm || !canvas || !meter_actor) return G_SOURCE_CONTINUE;

	MeterActor* m = meter_actor;

	static struct timeval prev_time;

	struct timeval new_time;
	gettimeofday(&new_time, NULL);
	time_t secs = new_time.tv_sec - prev_time.tv_sec;
	suseconds_t usec;
	if(new_time.tv_usec > prev_time.tv_usec){
		usec = new_time.tv_usec - prev_time.tv_usec;
	}else{
		secs -= 1;
		usec = new_time.tv_usec + 1000000 - prev_time.tv_usec;
	}
	//dbg(0, "%lu:%06lu", secs, usec);
	prev_time = new_time;

	/*
	 *  Use dB range -70 to +6
	 */
	float db_xlate(float db) { return MAX(0.0, (db - 6 + 76.0) / 76.0); }

	bool alive = false;

	int i = 0;
	AMIter iter;
	channel_iter_init(&iter);
	AMChannel* channel;
	while((channel = channel_next(&iter))){
		AMTrack* track = am_track_list_find_audio_by_idx(song->tracks, channel->core_index);
		if(track && !am_track__is_master(track)){
			Channel* channel = &m->channels[i];
			g_return_val_if_fail(i < song->channels->array->len + 1, TIMER_STOP);

			float level = db_xlate(am_track__get_meterlevel(track));

			channel->level = level;
			channel->peak = MAX(level, channel->peak - (0.0000001 * usec));
			channel->max = MAX(level, channel->max);

			if(channel->ttl > 0){
				alive = true;
				channel->ttl--;
			}
			i++;
		}
	}

	float master_level[2];
	am_track__get_meterlevel_master(master_level);
	for(int j=0;j<2;j++){
		m->channels[i].level = db_xlate(master_level[j]);
		m->channels[i].peak = MAX(m->channels[i].level, m->channels[i].peak - (0.0000001 * usec));
		m->channels[i].max = MAX(m->channels[i].level, m->channels[i].max);
		i++;
	}

	if(i != m->n_tracks) pwarn("n_tracks %i", i);

	agl_actor__invalidate((AGlActor*)meter_actor);

	if(!alive){
		timer = 0;
		return G_SOURCE_REMOVE;
	}

	return G_SOURCE_CONTINUE;
}


static void
rebuild_tracks ()
{
	MeterActor* m = meter_actor;
	g_return_if_fail(m);

	void on_meter_change (Observable* observable, AMVal val, gpointer channel)
	{
		((Channel*)channel)->ttl = 80;

		if(!timer)
			timer = g_timeout_add(50, (gpointer)_ayyi_meter_periodic_update, NULL);
	}

	for(int i = 0; i < meter_actor->n_tracks - 2; i++){ // ignoring master out
		Channel* channel = &meter_actor->channels[i];
		observable_unsubscribe(channel->track->meterval, on_meter_change, NULL);
	}

	g_free0(m->channels);

	int n = song->channels->array->len + 2; // extra because master is stereo

	m->channels = g_malloc0(sizeof(Channel) * n);

	dbg(2, "n_channels=%i", song->channels->array->len);

	int i = 0;
	AMIter iter;
	channel_iter_init(&iter);
	AMChannel* channel;
	while((channel = channel_next(&iter))){
		g_return_if_fail(i < song->channels->array->len + 1);

		AMTrack* track = am_track_list_find_audio_by_idx(song->tracks, channel->core_index);
		if(track && !am_track__is_master(track)){

			m->channels[i].track = track;
			m->channels[i].name = track->name;

			am_track__enable_metering(track);
			observable_subscribe(track->meterval, on_meter_change, &m->channels[i]);

			i++;
		}
	}

	m->channels[i++].name = g_strdup("OUT L");
	m->channels[i++].name = g_strdup("OUT R");

	m->n_tracks = i;

	observable_set(m->tracklist, (AMVal){.c = (void*)m->channels});
}


static void
meter_on_track_add (GObject* _song, AMTrack* tr, gpointer user_data)
{
	// TODO there is code somewhere for throttled signal handling. where?

	PF;
	void queue_rebuild ()
	{
		static bool queued = false;

		gboolean do_rebuild()
		{
			rebuild_tracks();
			queued = false;
			return G_SOURCE_REMOVE;
		}

		if(!queued){
			g_idle_add(do_rebuild, NULL);
			queued = true;
		}
	}

	queue_rebuild();
}


static void
meter_on_track_delete (GObject* _song, GList* tracks, gpointer user_data)
{
	rebuild_tracks();
}


static void
observer__transport_play ()
{
	for(int i=0;i<meter_actor->n_tracks;i++){
		meter_actor->channels[i].max = 0.0;
	}
}


#ifdef NOT_USED
static void
print_widget_tree (GtkWidget* widget)
{
	void print_children (GtkWidget* widget, int* depth)
	{
		if(GTK_IS_CONTAINER(widget)){
			GList* children = gtk_container_get_children(GTK_CONTAINER(widget));
			for(GList* l=children;l;l=l->next){
				GtkWidget* child = l->data;
				char indent[128];
				snprintf(indent, 127, "%%%is%%s\n", *depth);
				printf(indent, " ", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(child)));
				if(GTK_IS_CONTAINER(widget)){
					(*depth)++;
					print_children(child, depth);
					(*depth)--;
				}
			}
		}
	}

	PF;
	int depth = 0;
	if(GTK_IS_CONTAINER(widget)){
		print_children(widget, &depth);
	}
}
#endif
