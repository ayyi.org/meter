/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __meter_actor_h__
#define __meter_actor_h__

#include "agl/typedefs.h"
#include "model/track.h"
#include "pipeline.h"

typedef struct
{
    AMTrack* track;
    char*    name;

    float    level;
    float    peak;
    float    max;

    int      ttl;
} Channel;

typedef struct
{
    PostActor   actor;
    int         n_tracks;
    Observable* tracklist;
    Channel*    channels;
} MeterActor;

AGlActor* meter_actor_new ();

#endif
