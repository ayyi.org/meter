/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2020-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __meter_actor_c__
#include "config.h"
#include "debug/debug.h"
#include "model/ayyi_model.h"
#include "agl/utils.h"
#include "agl/ext.h"
#include "agl/shader.h"
#include "agl/text/renderer.h"
#include "nodes/cache.h"          // TODO move upstream
#include "post.h"
#include "nodes/meter.h"

#define LEFT_BORDER 4 // match the meters node which has to allow for convolution effects

AGlActor* labels_y_new ();
static AGlActorClass actor_class = {0, "LabelsY", (AGlActorNew*)labels_y_new};


AGlActorClass*
labels_y_get_class ()
{
	static bool init_done = false;

	if(!init_done){
		agl_actor_class__add_behaviour(&actor_class, cache_get_class());

		init_done = true;
	}

	return &actor_class;
}


static void
line (AGlActor* actor, const char* label, float position)
{
	float width = agl_actor__width(actor);

	AGl* agl = agl_get_instance();

	agl->shaders.plain->uniform.colour = 0xffffff77;
	agl_use_program((AGlShader*)agl->shaders.plain);
	agl_rect(LEFT_BORDER, position, width - 18., 1);

	agl_print(width - 13, position - 5., 0., 0xffffff77, label);
}


static bool
labels_y_draw (AGlActor* actor)
{
	float height = agl_actor__height(actor) - VBORDER;

	agl_set_font("Roboto", 6, PANGO_WEIGHT_SEMIBOLD);

	//line(actor, "+6",  VBORDER + 0);
	line(actor, " 0",  VBORDER + 6. * height / 76.);
	line(actor, "-20", VBORDER + 6. + 20. * height / 76.);
	line(actor, "-40", VBORDER + 6. + 40. * height / 76.);
	line(actor, "-60", VBORDER + 6. + 60. * height / 76.);

	return true;
}


static void
labels_y_set_size (AGlActor* actor)
{
	actor->region = (AGlfRegion){VBORDER, 0., actor->parent->region.x2, agl_actor__height(actor->parent) - 15};
}


static void
labels_init (AGlActor* actor)
{
	labels_y_set_size(actor);
}


AGlActor*
labels_y_new ()
{
	labels_y_get_class ();

	AGlActor* actor = (AGlActor*)agl_actor__new(MeterActor,
		.actor = {
			.actor = {
				.class = &actor_class,
				.name = actor_class.name,
				.init = labels_init,
				.set_size = labels_y_set_size,
				.paint = labels_y_draw,
			}
		}
	);

	return actor;
}

