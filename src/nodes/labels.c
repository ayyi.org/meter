/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2020-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __meter_actor_c__
#include "config.h"
#include "debug/debug.h"
#include "model/ayyi_model.h"
#include "agl/utils.h"
#include "agl/ext.h"
#include "agl/text/renderer.h"
#include "nodes/cache.h"          // TODO move upstream
#include "post.h"
#include "nodes/meter.h"

#define LEFT_BORDER 4 // match the meters node which has to allow for convolution effects
#define cache() ((CacheBehaviour*)actor->behaviours[0])

AGlActor* labels_new ();
static AGlActorClass actor_class = {0, "Labels", (AGlActorNew*)labels_new};


AGlActorClass*
labels_get_class ()
{
	static bool init_done = false;

	if(!init_done){
		agl_actor_class__add_behaviour(&actor_class, cache_get_class());

		init_done = true;
	}

	return &actor_class;
}


static bool
labels_draw (AGlActor* actor)
{
	MeterActor* m = actor->parent->children->next->data;

	agl_set_font("Roboto", 6, PANGO_WEIGHT_SEMIBOLD);

	float spacing = agl_actor__width(actor) / (m->n_tracks * 5);
	float bar_width = spacing * 4;

	for(int i=0;i<m->n_tracks;i++){
		float x = LEFT_BORDER + i * (bar_width + spacing);

		if(!builder()->target){
			agl_push_clip(x, 0, bar_width, 280);
		}else{
			agl_push_clip(x, 0, bar_width, 20);
		}

		agl_print(x, 0, 0, 0x999999ff, "%s", m->channels[i].name);

		agl_pop_clip();
	}
	return true;
}


static void
labels_set_size (AGlActor* actor)
{
	float top = agl_actor__height(actor->parent) - 15;
	actor->region = (AGlfRegion){VBORDER, top, actor->parent->region.x2 - 2 * VBORDER, top + 20};
}


static void
labels_init (AGlActor* actor)
{
	labels_set_size(actor);

	MeterActor* m = actor->parent->children->next->data;
	cache_behaviour_add_dependency(cache(), actor, m->tracklist);
}


AGlActor*
labels_new ()
{
	labels_get_class ();

	AGlActor* actor = (AGlActor*)agl_actor__new(MeterActor,
		.actor = {
			.actor = {
				.class = &actor_class,
				.name = actor_class.name,
				.init = labels_init,
				.set_size = labels_set_size,
				.paint = labels_draw,
			}
		}
	);

	return actor;
}

