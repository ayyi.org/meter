/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __meter_actor_c__
#include "config.h"
#include "debug/debug.h"
#include "agl/utils.h"
#include "agl/ext.h"
#include "agl/shader.h"
#include "agl/behaviours/key.h"
#include "post.h"
#include "nodes/meter.h"

#define LEFT_BORDER 4 // allow for convolution effects

static bool enable_filtering = true;
static ActorKeyHandler toggle_filtering;

static ActorKey keys[] = {
	{XK_f, toggle_filtering},
	{0,}
};

#define KEYS(A) ((KeyBehaviour*)A->behaviours[0])

static AGlActorClass actor_class = {0, "Meter", (AGlActorNew*)meter_actor_new};

static void actor_create_programs (PostActor*);


AGlActorClass*
meter_get_class ()
{
	static bool init_done = false;

	if(!init_done){
		agl_actor_class__add_behaviour(&actor_class, key_get_class());
		init_done = true;
	}

	return &actor_class;
}


static bool
meter_actor_draw (AGlActor* actor)
{
	MeterActor* m = (MeterActor*)actor;
	PostActor* post = (PostActor*)actor;

	ActorPipeline* pipeline = post->pipeline;
	bool enable_post = enable_filtering && post->post && post->post->in;
	if(enable_post){
		agl_draw_to_fbo(post->post->in) {
		}
	}
	//glClearColor(0, 0, 0, 0); // TODO use clearcolor to preserve alpha
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	gl_warn("start");

	float spacing = agl_actor__width(actor) / (m->n_tracks * 5);
	float bar_width = spacing * 4;//(((float)actor->w) * 4) / (m->n_tracks * 5);
	float height = agl_actor__height(actor);

	glPushMatrix(); /* modelview matrix */
		glTranslatef(LEFT_BORDER, 0.0, 0.0);

		// black background
#if 0
		AGl* agl = agl_get_instance();
		for(int i=0;i<m->n_tracks;i++){
			agl->shaders.plain->uniform.colour = 0x000000ff;
			agl_use_program((AGlShader*)agl->shaders.plain);
			agl_rect(i * (bar_width + spacing), 0, bar_width, 90);
		}
#endif

		pipeline->set_state();

		if(pipeline->draw_all){
			pipeline->draw_all(
				bar_width,
//				m->levels.levels,
NULL,
//				m->levels.peaks,
NULL,
//				m->levels.max,
NULL,
				m->n_tracks
			);
		}else{
			for(int i=0;i<m->n_tracks;i++){
				pipeline->draw(post,
					bar_width,
					height * m->channels[i].level,
					height * m->channels[i].peak,
					height * m->channels[i].max
				);

				glTranslatef(bar_width + spacing, 0.0, 0);
			}
		}
	glPopMatrix();

	if(enable_post){
		{
		} agl_end_draw_to_fbo;

		post_render(post);
	}

	return true;
}


static void
meter_init (AGlActor* actor)
{
	PostActor* post = (PostActor*)actor;

	// TODO make pipeline a behaviour
	if(post->pipeline) post->pipeline->init();

	actor_create_programs(post);
}


static void
meter_set_size (AGlActor* actor)
{
	g_return_if_fail(actor);

	PostActor* post = (PostActor*)actor;

	actor->region = (AGlfRegion){VBORDER, VBORDER, actor->parent->region.x2 - 2 * VBORDER, actor->parent->region.y2 - 2 * VBORDER};

	int w = agl_actor__width(actor);
	int h = agl_actor__height(actor);

	g_return_if_fail(w > 0);
	g_return_if_fail(h > 0);

	if(post->post){
		Post* pp = post->post;

		bool fbo_changed = !pp->in || agl_power_of_two(pp->in->width) != agl_power_of_two(agl_actor__width(actor)) || agl_power_of_two(pp->in->height) != agl_power_of_two(agl_actor__height(actor));

		if(fbo_changed){
			if(pp->in) agl_fbo_free(pp->in);
			if(pp->out) agl_fbo_free(pp->out);
			pp->in = agl_fbo_new(w, h, 0, 0);
			pp->out = agl_fbo_new(w, h, 0, 0);
		}else{
			if(pp->in){
				pp->in->height = h;
				pp->in->width = w;
			}
			if(pp->out){
				pp->out->height = h;
				pp->out->width = w;
			}
		}
	}

	if(post->pipeline) post->pipeline->set_size(post, w, h);
}


AGlActor*
meter_actor_new ()
{
	meter_get_class();

	AGlActor* actor = (AGlActor*)agl_actor__new(MeterActor,
		.actor = {
			.actor = {
				.class = &actor_class,
				.name = actor_class.name,
				.init = meter_init,
				.set_size = meter_set_size,
				.paint = meter_actor_draw,
			}
		}
	);
	PostActor* post = (PostActor*)actor;

	KEYS(actor)->keys = &keys;

	extern PipelineNew pipeline1_new, pipeline2_new, pipeline3_new;
	PipelineNew* pipeline = pipeline1_new;
	post->pipeline = pipeline(post);

	return actor;
}


static void
actor_create_programs (PostActor* actor)
{
	g_return_if_fail(actor);

	AGlShader** programs = actor->programs;
	if(programs){
		for(int i=0;programs[i];i++){
			agl_create_program(programs[i]);
		}
	}

#ifdef DEBUG
	gl_warn("before %s", ((AGlActor*)actor)->name);
#endif
}


static bool
toggle_filtering (AGlActor* actor, GdkModifierType modifier)
{
	enable_filtering = !enable_filtering;
	dbg(1, "filtering=%i", enable_filtering);

	return AGL_HANDLED;
}
