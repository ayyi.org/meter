/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __meter_actor_c__
#include "config.h"
#include "debug/debug.h"
#include "model/ayyi_model.h"
#include "agl/utils.h"
#include "agl/shader.h"
#include "nodes/cache.h"          // TODO move upstream
#include "../meter.h"

static AGl* agl = NULL;

AGlActor* time_actor_new ();
static AGlActorClass actor_class = {0, "Time", (AGlActorNew*)time_actor_new};


#ifdef USE_SONG_SERVICE // time not available if only using the Mixer service

static bool
time_draw ()
{
	glActiveTexture(GL_TEXTURE0);

	char spp[16];
	ayyi_transport_get_spp_string(spp);
	agl_set_font("Roboto", 18, PANGO_WEIGHT_SEMIBOLD);
	agl_print_with_background(0, 0, 0., 0xcccccc80, 0x33333377, "%s", spp);

	return true;
}


static void
on_song_periodic_update (GObject* _, gpointer actor)
{
	agl_actor__invalidate(actor);
}


AGlActor*
time_actor_new ()
{
	if(!agl){
		agl = agl_get_instance();
#if 0
		agl_actor_class__add_behaviour(&actor_class, cache_get_class());
#endif
	}

	AGlActor* actor = agl_actor__new(AGlActor,
		.class = &actor_class,
		.name = actor_class.name,
		.paint = time_draw,
		.region = {VBORDER, VBORDER, 200, 32},
	);

	am_song__connect("periodic-update", G_CALLBACK(on_song_periodic_update), actor);

	return actor;
}

#endif
