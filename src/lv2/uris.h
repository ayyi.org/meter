/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __uris_h__
#define __uris_h__

#include "lv2/atom/atom.h"
#include "lv2/atom/forge.h"
#include "lv2/parameters/parameters.h"
#include "lv2/urid/urid.h"

#define METER_URI "http://ayyi.org/plugins/meter"
#define METER_UI_URI "http://ayyi.org/plugins/gui/gtk2-gui"


typedef struct {
	// URIs defined in LV2 specifications
	LV2_URID atom_Vector;
	LV2_URID atom_Float;
	LV2_URID atom_Int;
	LV2_URID atom_eventTransfer;
	LV2_URID param_sampleRate;

	/* URIs defined for this plugin.  It is best to re-use existing URIs as
	   much as possible, but plugins may need more vocabulary specific to their
	   needs.  These are used as types and properties for plugin:UI
	   communication, as well as for saving state. */
	LV2_URID ui_On;
} MeterLV2URIs;


static inline void
map_sco_uris (LV2_URID_Map* map, MeterLV2URIs* uris)
{
	uris->atom_Vector        = map->map(map->handle, LV2_ATOM__Vector);
	uris->atom_Float         = map->map(map->handle, LV2_ATOM__Float);
	uris->atom_Int           = map->map(map->handle, LV2_ATOM__Int);
	uris->atom_eventTransfer = map->map(map->handle, LV2_ATOM__eventTransfer);
	uris->param_sampleRate   = map->map(map->handle, LV2_PARAMETERS__sampleRate);

	/* Note the convention that URIs for types are capitalized, and URIs for
	   everything else (mainly properties) are not, just as in LV2
	   specifications. */
	uris->ui_On     = map->map(map->handle, METER_UI_URI "#UIOn");
}

#endif
