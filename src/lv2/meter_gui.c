/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define _GNU_SOURCE
#include "config.h"
#include <stdio.h>
#include <lv2.h>
#include <lv2/lv2plug.in/ns/ext/atom/atom.h>
#include <lv2/lv2plug.in/ns/ext/atom/forge.h>
#include <lv2/lv2plug.in/ns/ext/urid/urid.h>
#include <lv2/lv2plug.in/ns/extensions/ui/ui.h>
#include "debug/debug.h"

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

#include "uris.h"
#include "../meter.h"
#include "../gl_canvas.h"
#include "panel.h"

#define PAD 2

typedef struct {
	uint32_t port_index;
	float value;
} port_event_t;

typedef struct control {
	LV2UI_Controller controller;
	LV2UI_Write_Function write;

	GSList* port_event_q;
	GtkWidget* vbox;

	MeterLV2URIs   uris;
	LV2_Atom_Forge forge;
} UI;


static void
about_button_cb (GtkWidget* about_button, gpointer data)
{
	#define S1 "<span size=\"small\">"
	#define S2 "</span>"
	#define XS1 "<span size=\"x-small\">"
	#define XS2 "</span>"

	GtkWidget* frame = gtk_frame_new(NULL);
	GtkWidget* label = gtk_label_new("");

	GtkWidget* dialog = gtk_dialog_new_with_buttons("About Ayyi Meter",
					     NULL,
					     GTK_DIALOG_DESTROY_WITH_PARENT,
					     GTK_STOCK_OK,
					     GTK_RESPONSE_NONE,
					     NULL);
	GtkWidget* content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialog));

	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
	gtk_label_set_markup(GTK_LABEL(label),
			     "<span size=\"x-large\" weight=\"heavy\">"
			     "IR</span><span size=\"x-large\">: LV2 Convolution Reverb\n"
			     "</span>"
			     S1 "version 1.2" S2
			     "\n\nCopyright (C) 2011 <b>Tim Orford</b>\n"
			     XS1 "\nAyyi is free software under the GNU GPL. There is ABSOLUTELY\n"
			     "NO WARRANTY, not even for MERCHANTABILITY or FITNESS\n"
			     "FOR A PARTICULAR PURPOSE." XS2 "\n\n"
			     "<small>Homepage: <b>http://ayyi.org/plugins/lv2/meter</b></small>");
	gtk_label_set_selectable(GTK_LABEL(label), TRUE);
	gtk_container_add(GTK_CONTAINER(frame), label);
	gtk_container_set_border_width(GTK_CONTAINER(frame), 2*PAD);

	g_signal_connect_swapped(dialog, "response", G_CALLBACK(gtk_widget_destroy), dialog);

	gtk_container_add(GTK_CONTAINER(content_area), frame);
	gtk_container_set_border_width(GTK_CONTAINER(dialog), 2*PAD);
	gtk_widget_show_all(dialog);
}


GtkWidget*
make_top_hbox (struct control* cp)
{
	GtkWidget* hbox = gtk_hbox_new(FALSE, PAD);
	GtkWidget* frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
	gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, PAD);
	GtkWidget* vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(frame), vbox);

	GtkWidget* hbox_wave = gtk_hbox_new(FALSE, PAD);
	gtk_box_pack_start(GTK_BOX(vbox), hbox_wave, TRUE, TRUE, PAD);
	
	GtkWidget* vbox_toggle = gtk_vbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox_wave), vbox_toggle, FALSE, TRUE, PAD);

	GtkWidget* hbox2 = gtk_hbox_new(FALSE, PAD);
	gtk_box_pack_start(GTK_BOX(vbox), hbox2, FALSE, FALSE, PAD);

	GtkWidget* about_button = gtk_button_new_with_label(" About ");
	g_signal_connect(about_button, "clicked", G_CALLBACK(about_button_cb), cp);
	gtk_box_pack_start(GTK_BOX(hbox2), about_button, FALSE, TRUE, PAD);

	return hbox;
}


static void port_event (LV2UI_Handle ui, uint32_t port_index, uint32_t buffer_size, uint32_t format, const void * buffer);

void
replay_func (gpointer data, gpointer user_data)
{
	port_event_t* pe = (port_event_t*)data;
	UI* cp = (struct control*)user_data;
	port_event((LV2UI_Handle)cp, pe->port_index, 0, 0, &pe->value);
	free(pe);
}

GtkWidget*
make_gui (UI* ui)
{
	printf("b::%s\n", __func__);

	ui->vbox = gtk_vbox_new(FALSE, PAD);

	GtkWidget* meter;
	if((meter = ayyi_meter_new(0, NULL, NULL))){
		gtk_box_pack_start(GTK_BOX(ui->vbox), meter, TRUE, TRUE, PAD);
	}

	return ui->vbox;
}


static void
cleanup (LV2UI_Handle ui)
{
	UI* cp = (UI*)ui;

#if 0
	while (cp->timeout_tag || cp->gui_load_timeout_tag) {
		
		gtk_main_iteration();
	}
#endif

	free(cp);
}


/*  Notify backend that UI is active:
 *  request state and enable data-transmission
 */
static void
ui_enable (LV2UI_Handle handle)
{
	UI* ui = (UI*)handle;

	uint8_t obj_buf[64];
	lv2_atom_forge_set_buffer(&ui->forge, obj_buf, 64);
	LV2_Atom_Forge_Frame frame;
	lv2_atom_forge_frame_time(&ui->forge, 0);
	LV2_Atom* msg = (LV2_Atom*)lv2_atom_forge_object(&ui->forge, &frame, 1, ui->uris.ui_On);
	lv2_atom_forge_pop(&ui->forge, &frame);

	ui->write(ui->controller, 0, lv2_atom_total_size(msg), ui->uris.atom_eventTransfer, msg);
}

static LV2UI_Handle
instantiate (const struct _LV2UI_Descriptor* descriptor,
				const char* plugin_uri,
				const char* bundle_path,
				LV2UI_Write_Function write,
				LV2UI_Controller controller,
				LV2UI_Widget* widget,
				const LV2_Feature* const* features)
{

	printf("instantiate('%s', '%s') called\n", plugin_uri, bundle_path);
	
	if (strcmp(plugin_uri, METER_URI) != 0) {
		fprintf(stderr, "IR_UI error: this GUI does not support plugin with URI %s\n", plugin_uri);
		goto fail;
	}

	UI* ui = (UI*)calloc(1, sizeof(UI));
	if (!ui) goto fail;

	ui->controller = controller;
	ui->write = write;

	*widget = (LV2UI_Widget)make_gui(ui);

	ui_enable((LV2UI_Handle)ui);

	return (LV2UI_Handle)ui;
 fail:
	return NULL;
}


static void
port_event (LV2UI_Handle ui, uint32_t port_index, uint32_t buffer_size, uint32_t format, const void * buffer)
{
	printf("port_event(%u, %f) called\n", (unsigned int)port_index, *(float *)buffer);

	if (format != 0) {
		return;
	}
}

static LV2UI_Descriptor descriptors[] = {
	{METER_UI_URI, instantiate, cleanup, port_event, NULL}
};

const LV2UI_Descriptor*
lv2ui_descriptor (uint32_t index)
{
	if (index >= sizeof(descriptors) / sizeof(descriptors[0])) {
		return NULL;
	}
	return descriptors + index;
}
