/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __meter_h__
#define __meter_h__

#include "config.h"
#include "agl/typedefs.h"
#include "agl/ext.h"
#include "pipeline.h"

#define VBORDER 8

typedef ActorPipeline* (PipelineNew)     (PostActor*);

GtkWidget* ayyi_meter_new (int argc, char** argv, void (*on_canvas_ready)());
void       canvas_destroy ();
GtkWidget* canvas_new     ();

#ifndef true
#define true  1
#define false 0
#endif

#ifndef g_free0
#define g_free0(var) ((var == NULL) ? NULL : (var = (g_free(var), NULL)))
#endif

#endif
