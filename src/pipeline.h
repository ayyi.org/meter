/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __pipeline_h__
#define __pipeline_h__

#include "agl/actor.h"

typedef struct _post      Post;
typedef struct _PostActor PostActor;


typedef void  (*PipelineInit)     ();
typedef void  (*PipelineSize)     (PostActor*, int, int);
typedef void  (*PipelineSetState) ();
typedef void  (*PipelineDraw)     (PostActor*, float bar_width, float, float peak, float);
typedef void  (*PipelineDrawAll)  (float bar_width, float*, float* peak, float*, int n);

typedef struct
{
	PipelineInit     init;
	PipelineSize     set_size;
	PipelineSetState set_state;
	PipelineDraw     draw;
	PipelineDrawAll  draw_all;
} ActorPipeline;

typedef void           (*ActorDraw)  (PostActor*, GtkWidget*);
typedef ActorPipeline* (PipelineNew) (PostActor*);

struct _PostActor
{
    AGlActor         actor;
	ActorDraw        draw;
	ActorPipeline*   pipeline;
	AGlShader**      programs;
	Post*            post;
};

#endif
