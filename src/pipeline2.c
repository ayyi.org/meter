/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*   This renderer uses the 'lines' shader.
*
*/
#include "config.h"
#include <stdlib.h>
#include <string.h>
#include "agl/utils.h"
#include "agl/ext.h"
#include "agl/fbo.h"
#include "model/ayyi_model.h"
#include "shader.h"
#include "post.h"
#include "meter.h"

static void set_uniforms();

extern AGlShaderText lines_text;

static AGlUniformInfo uniforms[] = {
   {"height", 1, GL_FLOAT, {0,}, -1},
   {"level", 1, GL_FLOAT, {0,}, -1},
   {"peak", 1, GL_FLOAT, {0,}, -1},
   {"peak2", 1, GL_FLOAT, {0,}, -1},
   END_OF_UNIFORMS
};

static AGlShader shader = SHADER("lines.vert", "lines.frag", uniforms, lines_text, set_uniforms);

typedef enum
{
   UNIFORM_HEIGHT = 0,
   UNIFORM_LEVEL,
   UNIFORM_PEAK,
   UNIFORM_PEAK2,
   UNIFORM_MAX,
} UniformType;

#define USE_BLUR_H
#define USE_BLUR_V
#include "shaders/info.c"


static void
init ()
{
}


static void
set_state ()
{
	if(agl_get_instance()->use_shaders) agl_use_program(&shader);
}


static int w = 0;
static int h = 0;

static void
set_size (PostActor* actor, int _w, int _h)
{
	// size of window. only useful for height.

	w = _w;
	h = _h;

	if(agl_get_instance()->use_shaders){
		sh_blur_h.uniform.texture_size = agl_power_of_two(actor->post->in->width);
		sh_blur_v.uniform.texture_size = agl_power_of_two(actor->post->in->height);
		sh_blur_h.uniform.gain = 1.2;
		sh_blur_v.uniform.gain = 1.2;
		sh_blur_h.uniform.amount = 0.7;
		sh_blur_v.uniform.amount = 0.7;
	}
}


static void
set_uniforms ()
{
}


static inline void
set_uniform_f (AGlShader* shader, int u, float* prev)
{
	AGlUniformInfo* uniform = &shader->uniforms[u];
	if(uniform->value[0] != *prev){
		glUniform1f(uniform->location, uniform->value[0]);
		*prev = uniform->value[0];
	}
}


static void
draw (PostActor* actor, float width, float height, float peak, float max)
{
	static float prev[UNIFORM_MAX] = {0,};

	shader.uniforms[UNIFORM_HEIGHT].value[0] = h;
	shader.uniforms[UNIFORM_LEVEL].value[0] = height;
	shader.uniforms[UNIFORM_PEAK].value[0] = peak;
	shader.uniforms[UNIFORM_PEAK2].value[0] = max;

	for(int i=0;i<UNIFORM_MAX;i++){
		set_uniform_f(&shader, i, &prev[i]);
	}

	agl_rect(0., 0., width, h);
}


#ifdef USE_BLUR_H
static void
post1 (PostActor* actor)
{
	post_filter(actor, (AGlShader*)&sh_blur_h);
}
#endif


#ifdef USE_BLUR_V
static void
post2 (PostActor* actor)
{
	post_filter_final(actor, (AGlShader*)&sh_blur_v);
}
#endif


ActorPipeline*
pipeline2_new (PostActor* actor)
{
	ActorPipeline* p = g_new0(ActorPipeline, 1);
	p->init = init;
	p->set_size = set_size;
	p->set_state = set_state;
	p->draw = draw;

#ifdef USE_BLUR_V
	Post* pp = actor->post = g_new0(Post, 1);
	pp->proc = g_list_append(pp->proc, post1);
	pp->proc = g_list_append(pp->proc, post2);
#endif

	agl_get_instance()->programs[AGL_APPLICATION_SHADER_2] = &shader;

	actor->programs = g_malloc0(sizeof(AGlShader) * 3);
#ifdef USE_BLUR_V
	actor->programs[0] = (AGlShader*)&sh_blur_h;
	actor->programs[1] = (AGlShader*)&sh_blur_v;
#endif

	return p;
}


