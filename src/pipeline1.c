/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*   This renderer uses the 'simple' shader - solid colour changing
*   from green to red
*
*/
#include "config.h"
#include "agl/utils.h"
#include "agl/ext.h"
#include "agl/fbo.h"
#include "model/ayyi_model.h"
#include "shader.h"
#include "post.h"

static void set_uniforms();
static void tv_set_uniforms();

extern AGlShaderText simple_text;
static AGlUniformInfo uniforms[] = {
   {"height", 1, GL_FLOAT, {0,}, -1},
   {"level", 1, GL_FLOAT, {0,}, -1},
   {"peak", 1, GL_FLOAT, {0,}, -1},
   {"peak2", 1, GL_FLOAT, {0,}, -1},
   END_OF_UNIFORMS
};
static AGlShader shader = {NULL, NULL, 0, uniforms, set_uniforms, &simple_text};
typedef enum
{
   UNIFORM_HEIGHT = 0,
   UNIFORM_LEVEL,
   UNIFORM_PEAK,
   UNIFORM_PEAK2,
   UNIFORM_MAX,
} UniformType;

#define USE_BLUR_H
#define USE_BLUR_V
#include "shaders/info.c"

extern AGlShaderText tv_text;
static AGlUniformInfo tv_uniforms[] = {
   {"texture", 1, GL_INT, {1, 0, 0, 0}, -1}, // GL_TEXTURE1
   END_OF_UNIFORMS
};
static AGlShader tv_shader = {NULL, NULL, 0, tv_uniforms, tv_set_uniforms, &tv_text};


static void
init ()
{
}


static void
set_state ()
{
	if(agl_get_instance()->use_shaders) agl_use_program(&shader);
}


static void
set_size (PostActor* actor, int _w, int _h)
{
#ifdef USE_BLUR_H
	if(agl_get_instance()->use_shaders){
		sh_blur_h.uniform.texture_size = agl_power_of_two(actor->post->in->width);
		sh_blur_v.uniform.texture_size = agl_power_of_two(actor->post->in->height);
		sh_blur_h.uniform.gain = 1.2;
		sh_blur_v.uniform.gain = 1.2;
		sh_blur_h.uniform.amount = 0.7;
		sh_blur_v.uniform.amount = 0.7;
	}
#endif
}


static void
set_uniforms ()
{
}


static void
tv_set_uniforms ()
{
}


static inline void
set_uniform_f (AGlShader* shader, int u, float* prev)
{
	AGlUniformInfo* uniform = &shader->uniforms[u];
	if(uniform->value[0] != *prev){
		glUniform1f(uniform->location, uniform->value[0]);
		*prev = uniform->value[0];
	}
}


static void
draw (PostActor* actor, float width, float height, float peak, float max)
{
	AGlActor* a = (AGlActor*)actor;

	float h = agl_actor__height(a);

	static float prev[UNIFORM_MAX] = {0,};

	shader.uniforms[UNIFORM_HEIGHT].value[0] = h;
	shader.uniforms[UNIFORM_LEVEL].value[0] = height;
	shader.uniforms[UNIFORM_PEAK].value[0] = peak;
	shader.uniforms[UNIFORM_PEAK2].value[0] = max;

	for(int i=0;i<UNIFORM_MAX;i++){
		set_uniform_f(&shader, i, &prev[i]);
	}

	agl_rect(0., 0., width, h);
}


static void
draw_no_shaders (PostActor* actor, float width, float height, float peak, float max)
{
	agl_rect (0., 0., width, agl_actor__height((AGlActor*)actor));
}


static void
post1 (PostActor* actor)
{
	post_filter(actor, &tv_shader);
}


#ifdef USE_BLUR_H
static void
post2 (PostActor* actor)
{
	post_filter(actor, (AGlShader*)&sh_blur_v);
}


static void
post3 (PostActor* actor)
{
	post_filter_final(actor, (AGlShader*)&sh_blur_h);
}
#endif


#if 0
extern AGlShader* sh_texture_tmp;

/*
 *  Pass-through post processor using for debugging
 */
static void
null_post (PostActor* actor)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	agl_use_program(sh_texture_tmp);
	glActiveTexture(GL_TEXTURE1);
	AglFBO* fbo = pp->in;
	agl_use_texture(fbo->texture);

	float fh = fbo->height;
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 1.0 - h/fh); glVertex2f(0, h);
		glTexCoord2f(1.0, 1.0 - h/fh); glVertex2f(w, h);
		glTexCoord2f(1.0, 1.0);        glVertex2f(w, 0);
		glTexCoord2f(0.0, 1.0);        glVertex2f(0, 0);
	glEnd();
}
#endif


ActorPipeline*
pipeline1_new (PostActor* actor)
{
	bool have_shaders = agl_get_instance()->use_shaders;

	ActorPipeline* p = AGL_NEW(ActorPipeline,
		.init = init,
		.set_size = set_size,
		.set_state = set_state,
		.draw = have_shaders ? draw : draw_no_shaders,
	);

	actor->post = AGL_NEW(Post,
		.proc = g_list_append(NULL, post1),
	);
#ifdef USE_BLUR_H
	Post* pp = actor->post;
	pp->proc = g_list_append(pp->proc, post2);
	pp->proc = g_list_append(pp->proc, post3);
#endif
#if 0
	pp->proc = g_list_append(NULL, null_post);
#endif

	agl_get_instance()->programs[AGL_APPLICATION_SHADER_1] = &shader;

	actor->programs = g_malloc0(sizeof(AGlShader*) * 4);
	actor->programs[0] = &tv_shader;
#ifdef USE_BLUR_H
	actor->programs[1] = (AGlShader*)&sh_blur_h;
	actor->programs[2] = (AGlShader*)&sh_blur_v;
#endif

	return p;
}
