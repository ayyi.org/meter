/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __glarea_h__
#define __glarea_h__

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "agl/actor.h"

G_BEGIN_DECLS


#define TYPE_GL_AREA (gl_area_get_type ())
#define GL_AREA(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GL_AREA, GlArea))
#define GL_AREA_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_GL_AREA, GlAreaClass))
#define IS_GL_AREA(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GL_AREA))
#define IS_GL_AREA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_GL_AREA))
#define GL_AREA_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GL_AREA, GlAreaClass))

typedef struct _GlArea GlArea;
typedef struct _GlAreaClass GlAreaClass;
typedef struct _GlAreaPrivate GlAreaPrivate;

struct _GlArea {
    GtkDrawingArea parent_instance;
    GlAreaPrivate* priv;
    AGlScene*      scene;
};

struct _GlAreaClass {
	GtkDrawingAreaClass parent_class;
};


GType   gl_area_get_type  (void) G_GNUC_CONST;
GlArea* gl_area_new       (void);
GlArea* gl_area_construct (GType object_type);


G_END_DECLS

#endif
