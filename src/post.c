/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __post_c__
#include "config.h"
#include <glib.h>
#include "agl/utils.h"
#include "model/ayyi_model.h"
#include "post.h"


void
post_render (PostActor* actor)
{
	Post* post = actor->post;

	for(GList* l=post->proc;l;l=l->next){
		Processor p = l->data;
		if(l->next){ agl_draw_to_fbo(post->out) {} }
		p(actor);
		if(l->next){
			{} agl_end_draw_to_fbo;

			AGlFBO* in = post->in;
			post->in = post->out;
			post->out = in;
		}
	}
	gl_warn("end");
}


static void
set_texture_unit (AGlShader* shader)
{
	AGlUniformInfo* info = shader->uniforms;
	if(info && (info->type == GL_INT)){
		// post processors are always texture based so it is safe to assume that this uniform is a texture.
		switch((int)info->value[0]){
			case 0:
				glActiveTexture(GL_TEXTURE0);
				break;
			case 1:
				glActiveTexture(GL_TEXTURE1);
				break;
			case 2:
				glActiveTexture(GL_TEXTURE2);
				break;
			default:
				break;
		}
	}
}


/*
 *  Copy a texture using the given shader.
 */
void
post_filter (PostActor* actor, AGlShader* shader)
{
	AGlFBO* fbo = actor->post->in;

	//glClearColor(0, 0, 0, 0); // TODO use clearcolor to preserve alpha
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	agl_use_program(shader);
	set_texture_unit(shader);

	float texture_width = (float)agl_power_of_two(fbo->width);
	float texture_height = (float)agl_power_of_two(fbo->height);

	float tw = (float)fbo->width / texture_width;
	float th = ((float)fbo->height) / texture_height;

	agl_textured_rect(fbo->texture, 0, 0, fbo->width, fbo->height, &(AGlQuad){0.0, th, tw, 0.0});

	gl_warn("end");
}


void
post_filter_final (PostActor* actor, AGlShader* shader)
{
	AGlFBO* fbo = actor->post->in;

	agl_use_program(shader);
	set_texture_unit(shader);

	agl_textured_rect(
		fbo->texture,
		0, 0,
		agl_actor__width((AGlActor*)actor), agl_actor__height((AGlActor*)actor),
		&(AGlQuad){0.0, ((float)fbo->height)/((float)agl_power_of_two(fbo->height)), ((float)fbo->width)/((float)agl_power_of_two(fbo->width)), 0.0}
	);
}


