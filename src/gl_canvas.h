/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __GL_CANVAS_H__
#define __GL_CANVAS_H__

#include <glib.h>
#include <gtk/gtk.h>
#include "pipeline.h"

G_BEGIN_DECLS


#define TYPE_GL_CANVAS (gl_canvas_get_type ())
#define GL_CANVAS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GL_CANVAS, GlCanvas))
#define GL_CANVAS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_GL_CANVAS, GlCanvasClass))
#define IS_GL_CANVAS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GL_CANVAS))
#define IS_GL_CANVAS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_GL_CANVAS))
#define GL_CANVAS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GL_CANVAS, GlCanvasClass))

typedef struct _GlCanvas GlCanvas;
typedef struct _GlCanvasClass GlCanvasClass;
typedef struct _GlCanvasPrivate GlCanvasPrivate;

struct _GlCanvas {
	GtkDrawingArea parent_instance;
	GlCanvasPrivate* priv;
	AGlActor* actor;
};

struct _GlCanvasClass {
	GtkDrawingAreaClass parent_class;
};


GType     gl_canvas_get_type     (void) G_GNUC_CONST;
GlCanvas* gl_canvas_new          (void);
GlCanvas* gl_canvas_construct    (GType object_type);
void      gl_canvas_print_config (GlCanvas* self);


G_END_DECLS

#endif
