/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __post_h__
#define __post_h__

#include "meter.h"
#include "agl/fbo.h"

typedef void (*Processor)(PostActor*);

struct _post
{
	AGlFBO* in;
	AGlFBO* out;
	GList*  proc; // Processor fns
};

void post_render       (PostActor*);
void post_filter       (PostActor*, AGlShader*);
void post_filter_final (PostActor*, AGlShader*);

#endif
