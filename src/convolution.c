/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include "agl/utils.h"
#include "agl/ext.h"

extern GLint kernel_size;

static void fill_convolution (GLfloat* k, GLfloat* scale);


void
setup_convolution (GLuint program)
{
	GLfloat* kernel = (GLfloat*)g_malloc(sizeof(GLfloat) * kernel_size);
	GLfloat* vec = (GLfloat*)g_malloc(sizeof(GLfloat) * kernel_size * 4);

	GLfloat scale;
	fill_convolution(kernel, &scale);

	// convert the kernel into a vec4
	for (int i = 0; i < kernel_size; ++i) {
		vec[i * 4 + 0] = kernel[i];
		vec[i * 4 + 1] = kernel[i];
		vec[i * 4 + 2] = kernel[i];
		vec[i * 4 + 3] = kernel[i];
	}

	GLuint loc;
	loc = glGetUniformLocation(program, "kernel_value"); glUniform4fv(loc, kernel_size, vec);
	loc = glGetUniformLocation(program, "scale_factor"); glUniform4f (loc, scale, scale, scale, scale);

	g_free(vec);
	g_free(kernel);
}


static void
fill_convolution (GLfloat* k, GLfloat* scale)
{
#ifdef SMALL_KERNEL
	k[0] = 1; k[1] = 2; k[2] = 1;
	k[3] = 2; k[4] = 4; k[5] = 2;
	k[6] = 1; k[7] = 2; k[8] = 1;

	*scale = 1./16.;
#else
	if(kernel_size == 25){
		k[ 0] = 1; k[ 1] =  4; k[ 2] =  7; k[ 3] =  4; k[ 4] = 1;  
		k[ 5] = 4; k[ 6] = 20; k[ 7] = 33; k[ 8] = 20; k[ 9] = 4;
		k[10] = 7; k[11] = 33; k[12] = 55; k[13] = 33; k[14] = 7;
		k[15] = 4; k[16] = 20; k[17] = 33; k[18] = 20; k[19] = 4;
		k[20] = 1; k[21] =  4; k[22] =  7; k[23] =  4; k[24] = 1;

		*scale = 1.0/331.0;
	}
	
	if(kernel_size == 49){
		k[ 0] = 0.00000067; k[ 1] = 0.00002292; k[ 2] = 0.00019117; k[ 3] = 0.00038771; k[ 4] = 0.00019117; k[ 5] = 0.00002292; k[ 6] = 0.00000067;
		k[ 7] = 0.00002292; k[ 8] = 0.00078633; k[ 9] = 0.00655965; k[10] = 0.01330373; k[11] = 0.00655965; k[12] = 0.00078633; k[13] = 0.00002292;
		k[14] = 0.00019117; k[15] = 0.00655965; k[16] = 0.05472157; k[17] = 0.11098164; k[18] = 0.05472157; k[19] = 0.00655965; k[20] = 0.00019117;
		k[21] = 0.00038771; k[22] = 0.01330373; k[23] = 0.11098164; k[24] = 0.22508352; k[25] = 0.11098164; k[26] = 0.01330373; k[27] = 0.00038771;
		k[28] = 0.00019117; k[29] = 0.00655965; k[30] = 0.05472157; k[31] = 0.11098164; k[32] = 0.05472157; k[33] = 0.00655965; k[34] = 0.00019117;
		k[35] = 0.00002292; k[36] = 0.00078633; k[37] = 0.00655965; k[38] = 0.01330373; k[39] = 0.00655965; k[40] = 0.00078633; k[41] = 0.00002292;
		k[42] = 0.00000067; k[43] = 0.00002292; k[44] = 0.00019117; k[45] = 0.00038771; k[46] = 0.00019117; k[47] = 0.00002292; k[48] = 0.00000067;

		*scale = 1.0;
	}
#endif
}

