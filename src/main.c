/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <getopt.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <dbus/dbus-glib.h>
#include "debug/debug.h"
#include "model/ayyi_model.h"
#include "p2p_client.h"

#define xembed_plug_new_for_display   gtk_plug_new_for_display
#define xembed_plug_get_embedded      gtk_plug_get_embedded
#define XEMBED_PLUG                   GTK_PLUG
#define xembed_plug_get_socket_window gtk_plug_get_socket_window
#define xembed_plug_get_id            gtk_plug_get_id
#define XembedPlug                    GtkPlug

#include "meter.h"

struct _p2p
{
	int         port;
	DBusGProxy* proxy;
} p2p = {0, NULL};

int             debug = 0;
bool            use_shaders = true;

GdkNativeWindow socket_id = 0;
GtkWidget*      window = NULL;

static void     ayyi_meter_on_realise    (GtkWidget*, gpointer);
static void     on_hierarchy_changed     (GtkWidget*, GtkWidget* previous_toplevel, gpointer);
static void     setup_peer_comms         ();
static void     quit                     ();


int
print_help ()
{
	printf("Usage: " PACKAGE_STRING "\n"
	     "  -s, --no-shaders                 Do not use Opengl shaders\n"
	     "  -x, --xid                        Xembed id of existing window to utilise\n"
	     "  -p, --port                       Port number of dbus p2p server\n"
	     "  -v, --version                    Show version information\n"
	     "  -h, --help                       Print this message\n"
	     "  -d, --debug     level            Output debug info to stdout\n\n"
	     "shortcuts: \n"
	     "  RETURN play\n"
	     "  SPACE  stop / RTZ\n"
	     "  q      quit\n"
		);
	return 1;
}


int
parse_opts (int argc, char* argv[])
{
	const char* optstring = "sx:p:hvd:";

	const struct option longopts[] = {
		{ "no-shaders", 0, 0, 's' },
		{ "xid", 1, 0, 'x' },
		{ "port", 1, 0, 'p' },
		{ "version", 0, 0, 'v' },
		{ "help", 0, 0, 'h' },
		{ "debug", 1, 0, 'd' },
		{ 0, 0, 0, 0 }
	};

	int option_index = 0;
	int c = 0;

	while (1) {
		c = getopt_long (argc, argv, optstring, longopts, &option_index);

		if (c == -1) {
			break;
		}

		switch (c) {
			case 0:
				break;

			case 's':
				printf("shaders disabled - FIXME broken following refactor\n");
				use_shaders = false;
				//agl_get_instance()->pref_use_shaders = false;
				break;

			case 'x':
				socket_id = atoi(optarg);
				printf("embedding in xid=%p\n", GINT_TO_POINTER(socket_id));
				break;

			case 'p':
				p2p.port = atoi(optarg);
				if(debug) printf("dbus port: %i\n", p2p.port);
				break;

			case 'v':
				printf("version " PACKAGE_VERSION "\n");
				exit (0);
				break;

			case 'h':
				print_help ();
				exit (0);
				break;
			case 'd':
				debug = _debug_ = atoi(optarg);
				break;

			default:
				return print_help();
		}
	}

	if (optind < argc) {
		//session_name = argv[optind++];
	}

	if(socket_id && !p2p.port) pwarn("dbus port not set.");

	return 0;
}


static void
__on_canvas_ready ()
{
	if(!socket_id){
		g_signal_connect(G_OBJECT(window), "delete-event", G_CALLBACK(quit), NULL);
	}else{
		gboolean ayyi_meter_on_delete (GtkWidget* widget, GdkEvent* event, gpointer user_data)
		{
			PF0;

			GError* error = NULL;
			gchar* echo;
			if (!org_ayyi_P2PObject_set_xid (p2p.proxy, "hello", &echo, &error)) {
				g_warning ("error calling set_xid: %s", error->message);
				g_error_free (error);
			}
			int _socket_id = atoi(echo);
			if(_socket_id != socket_id){
				socket_id = _socket_id;
				dbg(0, ">>> creating new plug... %x", socket_id);
				//g_object_ref(canvas);
				canvas_destroy();
				gtk_widget_destroy(window);

				window = xembed_plug_new_for_display(gdk_display_get_default(), socket_id);
				gtk_widget_show(window);
				/*
				dbg(0, ">>> reparenting...");
				if(canvas->parent){
					gtk_widget_reparent(canvas, window);
				}else{
					gtk_container_add(GTK_CONTAINER(window), canvas);
				}
				*/

				gboolean ayyi_meter_on_after_reparent (gpointer user)
				{
					GtkWidget* canvas = canvas_new();
					gtk_widget_show(canvas);
					gtk_container_add(GTK_CONTAINER(window), canvas);

					return TIMER_STOP;
				}

				g_timeout_add(2000, ayyi_meter_on_after_reparent, NULL);
			}

			return AGL_HANDLED;
		}
		g_signal_connect(G_OBJECT(window), "delete-event", G_CALLBACK(ayyi_meter_on_delete), NULL);
	}
}


int
main (int argc, char** argv)
{
	set_log_handlers();

	parse_opts (argc, argv);

	gtk_init(&argc, &argv);

	window = socket_id
		? xembed_plug_new_for_display(gdk_display_get_default(), socket_id)
		: gtk_window_new(GTK_WINDOW_TOPLEVEL);

	if(socket_id){
		dbg(2, "socket is widget? %i", GTK_IS_WIDGET(window));
		dbg(2, "embedded=%i", xembed_plug_get_embedded(XEMBED_PLUG(window)));
	}

#if 0
	void on_embedded(XembedPlug* plug, gpointer user_data)
	{
		dbg(0, "!! <<<<<<<<<<<<<<<<<<<<<<<<<<<< NEVER GET HERE");
	}

	if(socket_id) g_signal_connect(window, "embedded", (GCallback)on_embedded, NULL);
#endif

	setup_peer_comms();

	g_signal_connect(window, "realize", G_CALLBACK(ayyi_meter_on_realise), NULL);
	g_signal_connect(window, "hierarchy-changed", G_CALLBACK(on_hierarchy_changed), NULL);

	gboolean key_press (GtkWidget* widget, GdkEventKey* event, gpointer user_data)
	{
		switch(event->keyval){
			case 113:
				quit();
				return AGL_HANDLED;
			default:
				break;
		}
		return AGL_NOT_HANDLED;
	}
	g_signal_connect(window, "key-press-event", G_CALLBACK(key_press), NULL);

	GtkWidget* meter;
	if(!(meter = ayyi_meter_new(argc, argv, __on_canvas_ready))) return EXIT_FAILURE;
	gtk_container_add(GTK_CONTAINER(window), meter);
	gtk_widget_show_all(window);

	void on_allocate (GtkWidget* widget, GtkAllocation* allocation, gpointer user_data)
	{
		gtk_widget_set_size_request(widget, 20, 20);
	}
	//FIXME allocate used to be on canvas - check
	g_signal_connect((gpointer)meter, "size-allocate", G_CALLBACK(on_allocate), NULL);

	gtk_main();

	return EXIT_SUCCESS;
}


static gboolean window_init_done = false;

static void
ayyi_meter_on_realise (GtkWidget* widget, gpointer user_data)
{
	PF;
	if(!GTK_WIDGET_REALIZED (widget)) return;
	//if(!GTK_WIDGET_REALIZED (canvas)) return;
	if(window_init_done) return;

	//gl_drawable = gtk_widget_get_gl_drawable(canvas);
	//gl_context  = gtk_widget_get_gl_context(canvas);

	//gl_init();
	//setup_projection(widget);

	window_init_done = true;
}


static void
on_hierarchy_changed (GtkWidget* widget, GtkWidget* previous_toplevel, gpointer user_data)
{
	PF;
	dbg(0, "...");
}


static void
setup_peer_comms ()
{
	// To generate client bindings:
	//make dbus_service

	PF;
	if(!p2p.port) return;

	GError* error = NULL;
	gchar* server_path = g_strdup_printf ("tcp:host=%s,port=%i", "localhost", p2p.port);
	DBusGConnection* connection = dbus_g_connection_open (server_path, &error);
	g_free (server_path);

	if(error){
		P_GERR;
		return;
	}

	g_return_if_fail(connection);

	p2p.proxy = dbus_g_proxy_new_for_peer (connection, "/", "org.ayyi.P2PObject");


	gchar* echo;
	if (!org_ayyi_P2PObject_echo (p2p.proxy, "hello", &echo, &error)) {
		g_warning ("error calling echo: %s", error->message);
		g_error_free (error);
	}
	else {
		dbg(1, "\"hello\" => \"%s\"", echo);
		g_free (echo);
	}
}


static void
quit ()
{
	ayyi_client_disconnect();
	g_object_unref0(p2p.proxy);

	exit(EXIT_SUCCESS);
}
