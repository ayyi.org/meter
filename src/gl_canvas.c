/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "config.h"
#include <glib.h>
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "pipeline.h"

#define TYPE_GL_CANVAS (gl_canvas_get_type ())
#define GL_CANVAS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GL_CANVAS, GlCanvas))
#define GL_CANVAS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_GL_CANVAS, GlCanvasClass))
#define IS_GL_CANVAS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GL_CANVAS))
#define IS_GL_CANVAS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_GL_CANVAS))
#define GL_CANVAS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GL_CANVAS, GlCanvasClass))

typedef struct _GlCanvas GlCanvas;
typedef struct _GlCanvasClass GlCanvasClass;
typedef struct _GlCanvasPrivate GlCanvasPrivate;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

struct _GlCanvas {
	GtkDrawingArea parent_instance;
	GlCanvasPrivate* priv;
	AGlActor* actor;
};

struct _GlCanvasClass {
	GtkDrawingAreaClass parent_class;
};

struct _GlCanvasPrivate {
	gboolean gl_initialised;
};


static GdkGLConfig* gl_canvas_glconfig = NULL;
static GlCanvas* gl_canvas_instance = NULL;

G_DEFINE_TYPE_WITH_PRIVATE (GlCanvas, gl_canvas, GTK_TYPE_DRAWING_AREA)
enum  {
	GL_CANVAS_DUMMY_PROPERTY
};
GlCanvas*   gl_canvas_new (void);
GlCanvas*   gl_canvas_construct (GType object_type);
static void gl_canvas_realize (GtkWidget* base);
static void gl_canvas_gl_init (GlCanvas* self);
static void gl_canvas_setup_projection (GlCanvas* self);
static void gl_canvas_unrealize (GtkWidget* base);
static void gl_canvas_size_allocate (GtkWidget* base, GdkRectangle* allocation);
static gboolean gl_canvas_expose_event (GtkWidget* base, GdkEventExpose* event);
void        gl_canvas_print_config (GlCanvas* self);
static void gl_canvas_print_config_attrib (GlCanvas* self, gchar* attrib_str, gint attrib, gboolean is_boolean);
static void gl_canvas_finalize (GObject* obj);


GlCanvas*
gl_canvas_construct (GType object_type)
{
	GlCanvas* self = (GlCanvas*) g_object_new (object_type, NULL);

	gtk_widget_add_events ((GtkWidget*) self, (gint) ((GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK) | GDK_POINTER_MOTION_MASK));
	gtk_widget_set_size_request ((GtkWidget*) self, 200, 100);
	gtk_widget_set_can_focus ((GtkWidget*) self, TRUE);

	if (!gl_canvas_glconfig) {
		gl_canvas_glconfig = gdk_gl_config_new_by_mode (GDK_GL_MODE_RGB | GDK_GL_MODE_DOUBLE);
	}

	gtk_widget_set_gl_capability ((GtkWidget*) self, gl_canvas_glconfig, NULL, TRUE, (gint) GDK_GL_RGBA_TYPE);
	gdk_gl_query_extension ();
	GlCanvas* _tmp3_ = g_object_ref (self);
	_g_object_unref0 (gl_canvas_instance);
	gl_canvas_instance = _tmp3_;

	void
	canvas_gl_init (AGlActor* actor)
	{
		GlCanvas* self = (GlCanvas*)((AGlScene*)actor)->gl.gdk.widget;

		gl_canvas_gl_init (self);
		gl_canvas_setup_projection (self);
	}

	self->actor = agl_actor__new_root((GtkWidget*)self);
	self->actor->name = "Root";
	self->actor->init = canvas_gl_init;

	return self;
}


GlCanvas* gl_canvas_new (void) {
	return gl_canvas_construct (TYPE_GL_CANVAS);
}


static void
gl_canvas_realize (GtkWidget* widget)
{
	GlCanvas* self = (GlCanvas*) widget;

	GTK_WIDGET_CLASS (gl_canvas_parent_class)->realize ((GtkWidget*) G_TYPE_CHECK_INSTANCE_CAST (self, GTK_TYPE_DRAWING_AREA, GtkDrawingArea));
}


static void
gl_canvas_unrealize (GtkWidget* widget)
{
	GlCanvas* self = (GlCanvas*)widget;
	GTK_WIDGET_CLASS (gl_canvas_parent_class)->unrealize ((GtkWidget*) G_TYPE_CHECK_INSTANCE_CAST (self, GTK_TYPE_DRAWING_AREA, GtkDrawingArea));
}


static void
gl_canvas_gl_init (GlCanvas* self)
{
	g_return_if_fail (self);

	if (self->priv->gl_initialised) {
		return;
	}

#if 0
	AGL_ACTOR_START_DRAW (self->actor);
	{
		if (!agl_shaders_supported ()) {
			pwarn ("shaders not supported");
			g_print ("Warning: this program prefers OpenGL 2.0\n");
		}

		if (_debug_) {
			GLint maxTexSize = 0;
			GLint _tmp3_ = 0;
			GLint texSize = 0;
			GLint _tmp4_ = 0;
			maxTexSize = (GLint) 0;
			glGetIntegerv ((GLenum) GL_MAX_TEXTURE_SIZE, &maxTexSize);
			_tmp3_ = maxTexSize;
			g_print ("GL_MAX_TEXTURE_SIZE               : %d\n", (gint) _tmp3_);
			texSize = (GLint) 0;
			glGetIntegerv ((GLenum) GL_MAX_RECTANGLE_TEXTURE_SIZE_ARB, &texSize);
			_tmp4_ = texSize;
			g_print ("GL_MAX_RECTANGLE_TEXTURE_SIZE_ARB : %d\n", (gint) _tmp4_);
		}
	}
	AGL_ACTOR_END_DRAW ();
#endif
	self->priv->gl_initialised = TRUE;
}


static void
gl_canvas_setup_projection (GlCanvas* self)
{
	g_return_if_fail (self);
	g_return_if_fail (self->actor);

	gint vx = 0;
	gint vy = 0;
	gint vw = ((GtkWidget*)self)->allocation.width;
	gint vh = ((GtkWidget*)self)->allocation.height;
	glViewport (vx, vy, (GLsizei)vw, (GLsizei)vh);

	glMatrixMode ((GLenum) GL_PROJECTION);
	glLoadIdentity ();

	gdouble left = 0.0;
	gdouble right = (gdouble) vw;
	gdouble top = vh;
	gdouble bottom = 0.0;
	glOrtho (left, right, top, bottom, 10.0, -100.0);
}


static void
gl_canvas_size_allocate (GtkWidget* widget, GdkRectangle* allocation)
{
	GlCanvas* self = (GlCanvas*)widget;
	g_return_if_fail (allocation);

	GTK_WIDGET_CLASS (gl_canvas_parent_class)->size_allocate ((GtkWidget*) G_TYPE_CHECK_INSTANCE_CAST (self, GTK_TYPE_DRAWING_AREA, GtkDrawingArea), allocation);

	self->actor->region = (AGlfRegion){.x2 = allocation->width, .y2 = allocation->height};
	agl_actor__set_size(self->actor);

	gl_canvas_setup_projection (self);
}


static gboolean
gl_canvas_expose_event (GtkWidget* base, GdkEventExpose* event)
{
	GlCanvas* self = (GlCanvas*)base;
	g_return_val_if_fail (event, FALSE);

	if (!gtk_widget_get_realized ((GtkWidget*) self)) {
		return TRUE;
	}

	AGL_ACTOR_START_DRAW (self->actor);
	{
		glClearColor ((GLclampf) 0.0f, (GLclampf) 0.0f, (GLclampf) 0.0f, (GLclampf) 1.0f);
		glClear ((GLbitfield) (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

		agl_actor__paint (self->actor);

		gdk_gl_drawable_swap_buffers (((AGlScene*)self->actor)->gl.gdk.drawable);
	}
	AGL_ACTOR_END_DRAW ();

	return TRUE;
}


void
gl_canvas_print_config (GlCanvas* self)
{
	const gchar* _tmp4_ = NULL;
	const gchar* _tmp12_ = NULL;
	const gchar* _tmp20_ = NULL;
	GdkGLConfig* _tmp21_ = NULL;
	gboolean _tmp22_ = FALSE;
	const gchar* _tmp23_ = NULL;
	const gchar* _tmp24_ = NULL;
	GdkGLConfig* _tmp25_ = NULL;
	gboolean _tmp26_ = FALSE;
	const gchar* _tmp27_ = NULL;
	g_return_if_fail (self);

	g_print ("\nOpenGL configuration:\n\n");

	gboolean _tmp2_ = gdk_gl_config_is_rgba (gl_canvas_glconfig);
	const gchar* _tmp0_ = NULL;
	if (_tmp2_) {
		_tmp0_ = "TRUE";
	} else {
		_tmp0_ = "FALSE";
	}
	const gchar* _tmp3_ = _tmp0_;
	g_print ("is_rgba (glconfig) = %s\n", _tmp3_);
	gboolean _tmp6_ = gdk_gl_config_is_double_buffered (gl_canvas_glconfig);
	if (_tmp6_) {
		_tmp4_ = "TRUE";
	} else {
		_tmp4_ = "FALSE";
	}
	const gchar* _tmp7_ = _tmp4_;
	g_print ("is_double_buffered (glconfig) = %s\n", _tmp7_);

	gboolean _tmp14_ = gdk_gl_config_has_alpha (gl_canvas_glconfig);
	if (_tmp14_) {
		_tmp12_ = "TRUE";
	} else {
		_tmp12_ = "FALSE";
	}
	g_print ("has_alpha (glconfig) = %s\n", _tmp12_);

	const gchar* _tmp16_ = NULL;
	if (gdk_gl_config_has_depth_buffer (gl_canvas_glconfig)) {
		_tmp16_ = "TRUE";
	} else {
		_tmp16_ = "FALSE";
	}
	g_print ("has_depth_buffer (glconfig) = %s\n", _tmp16_);
	_tmp21_ = gl_canvas_glconfig;
	_tmp22_ = gdk_gl_config_has_stencil_buffer (_tmp21_);
	if (_tmp22_) {
		_tmp20_ = "TRUE";
	} else {
		_tmp20_ = "FALSE";
	}
	_tmp23_ = _tmp20_;
	g_print ("has_stencil_buffer (glconfig) = %s\n", _tmp23_);
	_tmp25_ = gl_canvas_glconfig;
	_tmp26_ = gdk_gl_config_has_accum_buffer (_tmp25_);
	if (_tmp26_) {
		_tmp24_ = "TRUE";
	} else {
		_tmp24_ = "FALSE";
	}
	_tmp27_ = _tmp24_;
	g_print ("has_accum_buffer (glconfig) = %s\n", _tmp27_);
	g_print ("\n");
	gl_canvas_print_config_attrib (self, "GDK_GL_USE_GL", (gint) GDK_GL_USE_GL, TRUE);
	gl_canvas_print_config_attrib (self, "GDK_GL_BUFFER_SIZE", (gint) GDK_GL_BUFFER_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_LEVEL", (gint) GDK_GL_LEVEL, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_RGBA", (gint) GDK_GL_RGBA, TRUE);
	gl_canvas_print_config_attrib (self, "GDK_GL_DOUBLEBUFFER", (gint) GDK_GL_DOUBLEBUFFER, TRUE);
	gl_canvas_print_config_attrib (self, "GDK_GL_AUX_BUFFERS", (gint) GDK_GL_AUX_BUFFERS, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_RED_SIZE", (gint) GDK_GL_RED_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_GREEN_SIZE", (gint) GDK_GL_GREEN_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_BLUE_SIZE", (gint) GDK_GL_BLUE_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_ALPHA_SIZE", (gint) GDK_GL_ALPHA_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_DEPTH_SIZE", (gint) GDK_GL_DEPTH_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_STENCIL_SIZE", (gint) GDK_GL_STENCIL_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_ACCUM_RED_SIZE", (gint) GDK_GL_ACCUM_RED_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_ACCUM_GREEN_SIZE", (gint) GDK_GL_ACCUM_GREEN_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_ACCUM_BLUE_SIZE", (gint) GDK_GL_ACCUM_BLUE_SIZE, FALSE);
	gl_canvas_print_config_attrib (self, "GDK_GL_ACCUM_ALPHA_SIZE", (gint) GDK_GL_ACCUM_ALPHA_SIZE, FALSE);
	g_print ("\n");
}


static void
gl_canvas_print_config_attrib (GlCanvas* self, gchar* attrib_str, gint attrib, gboolean is_boolean)
{
	gint value = 0;
	gchar* _tmp0_ = NULL;
	GdkGLConfig* _tmp1_ = NULL;
	gint _tmp2_ = 0;
	gint _tmp3_ = 0;
	gboolean _tmp4_ = FALSE;
	g_return_if_fail (self != NULL);
	_tmp0_ = attrib_str;
	g_print ("%s = ", (const gchar*) _tmp0_);
	_tmp1_ = gl_canvas_glconfig;
	_tmp2_ = attrib;
	_tmp4_ = gdk_gl_config_get_attrib (_tmp1_, _tmp2_, &_tmp3_);
	value = _tmp3_;
	if (_tmp4_) {
		gboolean _tmp5_ = FALSE;
		_tmp5_ = is_boolean;
		if (_tmp5_) {
			const gchar* _tmp6_ = NULL;
			gint _tmp7_ = 0;
			const gchar* _tmp8_ = NULL;
			_tmp7_ = value;
			if ((gboolean) _tmp7_) {
				_tmp6_ = "TRUE";
			} else {
				_tmp6_ = "FALSE";
			}
			_tmp8_ = _tmp6_;
			g_print ("%s\n", _tmp8_);
		} else {
			gint _tmp9_ = 0;
			_tmp9_ = value;
			g_print ("%d\n", _tmp9_);
		}
	} else {
		gchar* _tmp10_ = NULL;
		_tmp10_ = attrib_str;
		g_print ("*** Cannot get %s attribute value\n", (const gchar*) _tmp10_);
	}
}


static void
gl_canvas_class_init (GlCanvasClass * klass)
{
	gl_canvas_parent_class = g_type_class_peek_parent (klass);

	GTK_WIDGET_CLASS (klass)->realize = gl_canvas_realize;
	GTK_WIDGET_CLASS (klass)->unrealize = gl_canvas_unrealize;
	GTK_WIDGET_CLASS (klass)->size_allocate = gl_canvas_size_allocate;
	GTK_WIDGET_CLASS (klass)->expose_event = gl_canvas_expose_event;

	G_OBJECT_CLASS (klass)->finalize = gl_canvas_finalize;
}


static void
gl_canvas_init (GlCanvas * self)
{
	self->priv = gl_canvas_get_instance_private(self);
}


static void
gl_canvas_finalize (GObject* obj)
{
	G_OBJECT_CLASS (gl_canvas_parent_class)->finalize (obj);
}

