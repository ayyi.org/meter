/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "agl/utils.h"
#include "gtkglext-1.0/gtk/gtkgl.h"
#include "glarea.h"

#define DIRECT TRUE

#define TYPE_GL_AREA            (gl_area_get_type ())
#define GL_AREA(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GL_AREA, GlArea))
#define GL_AREA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_GL_AREA, GlAreaClass))
#define IS_GL_AREA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GL_AREA))
#define IS_GL_AREA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_GL_AREA))
#define GL_AREA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GL_AREA, GlAreaClass))

typedef struct _GlArea GlArea;
typedef struct _GlAreaClass GlAreaClass;
typedef struct _GlAreaPrivate GlAreaPrivate;

static gpointer gl_area_parent_class = NULL;

GType gl_area_get_type (void) G_GNUC_CONST;
enum  {
	GL_AREA_0_PROPERTY
};

GlArea*     gl_area_new       (void);
GlArea*     gl_area_construct (GType);
static void gl_area_realize   (GtkWidget*);
static void gl_area_unrealize (GtkWidget*);
static bool gl_area_event     (GtkWidget*, GdkEvent*);


GlArea*
gl_area_construct (GType object_type)
{
	return (GlArea*) g_object_new (object_type, NULL);
}


GlArea*
gl_area_new (void)
{
	GlArea* area = gl_area_construct (TYPE_GL_AREA);
	area->scene = (AGlScene*)agl_actor__new_root((GtkWidget*)area);

	gtk_widget_set_can_focus ((GtkWidget*)area, true);
	gtk_widget_add_events ((GtkWidget*)area, (gint) ((GDK_KEY_PRESS_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK) | GDK_POINTER_MOTION_MASK));

	return area;
}


static void
gl_area_realize (GtkWidget* widget)
{
	static GdkGLConfig* config = NULL;

	//if(!config && !(config = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGBA | GDK_GL_MODE_DEPTH | GDK_GL_MODE_STENCIL | GDK_GL_MODE_DOUBLE))){
	if(!config && !(config = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGBA | GDK_GL_MODE_DOUBLE | GDK_GL_MODE_DEPTH))){
		perr ("Cannot initialise gtkglext");
		return;
	}
	gtk_widget_set_gl_capability(widget, config, agl_get_gl_context(), DIRECT, GDK_GL_RGBA_TYPE);

	GTK_WIDGET_CLASS(gl_area_parent_class)->realize (widget);
}


static void
gl_area_unrealize (GtkWidget* widget)
{
	PF;
	GTK_WIDGET_CLASS(gl_area_parent_class)->unrealize (widget);
}


static void
gl_area_size_allocate (GtkWidget* widget, GtkAllocation* allocation)
{
	GlArea* area = (GlArea*)widget;
	AGlfRegion* r = &((AGlActor*)area->scene)->region;

	GTK_WIDGET_CLASS (gl_area_parent_class)->size_allocate ((GtkWidget*) G_TYPE_CHECK_INSTANCE_CAST (widget, GTK_TYPE_DRAWING_AREA, GtkDrawingArea), allocation);

	if(r->x2 != allocation->width || r->y2 != allocation->height){
		*r = (AGlfRegion){.x2 = allocation->width, .y2 = allocation->height};
		agl_actor__set_size((AGlActor*)area->scene);
	}
}


static gboolean
gl_area_expose_event (GtkWidget* widget, GdkEventExpose* event)
{
	agl_actor__on_expose(widget, event, ((GlArea*)widget)->scene);

	return true;
}


static void
gl_area_class_init (GlAreaClass* klass)
{
	gl_area_parent_class = g_type_class_peek_parent (klass);

	((GtkWidgetClass*)klass)->realize = (void (*) (GtkWidget *)) gl_area_realize;
	((GtkWidgetClass*)klass)->unrealize = gl_area_unrealize;
	((GtkWidgetClass*)klass)->event = (gboolean (*) (GtkWidget *, GdkEvent*)) gl_area_event;
	((GtkWidgetClass*)klass)->size_allocate = gl_area_size_allocate;
	((GtkWidgetClass*)klass)->expose_event = gl_area_expose_event;
}


static void
gl_area_instance_init (GlArea* self)
{
}


GType
gl_area_get_type (void)
{
	static volatile gsize gl_area_type_id__volatile = 0;
	if (g_once_init_enter (&gl_area_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (GlAreaClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) gl_area_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (GlArea), 0, (GInstanceInitFunc) gl_area_instance_init, NULL };
		GType gl_area_type_id;
		gl_area_type_id = g_type_register_static (GTK_TYPE_DRAWING_AREA, "GlArea", &g_define_type_info, 0);
		g_once_init_leave (&gl_area_type_id__volatile, gl_area_type_id);
	}
	return gl_area_type_id__volatile;
}


static bool
gl_area_event (GtkWidget* widget, GdkEvent* event)
{
	g_return_val_if_fail (event, FALSE);

	return agl_actor__on_event(((GlArea*)widget)->scene, event);
}

