/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "agl/utils.h"
#include "agl/shader.h"
#include "shaders/shaders.c"

static AGlUniformInfo uniforms[] = {
   {"tex2d", 1, GL_INT, {0,}, -1}, // 0 corresponds to glActiveTexture(GL_TEXTURE0);
   END_OF_UNIFORMS
};
/*
 *  Uncomment the shader files to load the shader files directly at runtime
 */
/*static */AGlShader sh_main = {NULL/*"shader.vert"*/, NULL/*"shader.frag"*/, 0, uniforms, NULL, &shader_text};

static void init_programs ();


static void
shaders_init ()
{
	PF;

	if(agl_get_instance()->use_shaders){
		agl_create_program(&sh_main);
		init_programs();
		agl_use_program(&sh_main);
	}
}


static void
init_programs ()
{
	PF;

	{
		struct Texture {
			GLuint id;
			GLfloat x;
			GLfloat y;
			GLint width;
			GLint height;
			GLenum format;
		};
		struct Texture texture = {
			.width = 128,
			.height = 128
		};

#ifdef SMALL_KERNEL
		float offsets[] = { 1.0 / texture.width,  1.0 / texture.height,
		                    0.0                ,  1.0 / texture.height,
		                   -1.0 / texture.width,  1.0 / texture.height,

		                    1.0 / texture.width,  0.0,
		                    0.0                ,  0.0,
		                   -1.0 / texture.width,  0.0,

		                    1.0 / texture.width, -1.0 / texture.height,
		                    0.0                , -1.0 / texture.height,
		                   -1.0 / texture.width, -1.0 / texture.height };
#else

		if(kernel_size == 25){
			//TODO this one is more or less general, and can replace the others.
			int x, y;
			float offsets[kernel_size * 2];

			for(y=0; y<5; y++) {
				for(x=0; x<5; x++) {
					offsets[2 * (x + y*5)    ] = ((float)(x - 2)) / (float)texture.width;
					offsets[2 * (x + y*5) + 1] = ((float)(y - 2)) / (float)texture.height;
				}
			}

			GLuint offsetLoc = glGetUniformLocation(sh_main.program, "offset");
			glUniform2fv(offsetLoc, kernel_size, offsets);
		}

		if(kernel_size == 49){
			float offsets[] = {
				 3.0 / texture.width,  3.0 / texture.height,
				 2.0 / texture.width,  3.0 / texture.height,
				 1.0 / texture.width,  3.0 / texture.height,
				 0.0 / texture.width,  3.0 / texture.height,
				-1.0 / texture.width,  3.0 / texture.height,
				-2.0 / texture.width,  3.0 / texture.height,
				-3.0 / texture.width,  3.0 / texture.height,

				 3.0 / texture.width,  2.0 / texture.height,
				 2.0 / texture.width,  2.0 / texture.height,
				 1.0 / texture.width,  2.0 / texture.height,
				 0.0 / texture.width,  2.0 / texture.height,
				-1.0 / texture.width,  2.0 / texture.height,
				-2.0 / texture.width,  2.0 / texture.height,
				-3.0 / texture.width,  2.0 / texture.height,

				 3.0 / texture.width,  1.0 / texture.height,
				 2.0 / texture.width,  1.0 / texture.height,
				 1.0 / texture.width,  1.0 / texture.height,
				 0.0 / texture.width,  1.0 / texture.height,
				-1.0 / texture.width,  1.0 / texture.height,
				-2.0 / texture.width,  1.0 / texture.height,
				-3.0 / texture.width,  1.0 / texture.height,

				 3.0 / texture.width,  0.0 / texture.height,
				 2.0 / texture.width,  0.0 / texture.height,
				 1.0 / texture.width,  0.0 / texture.height,
				 0.0 / texture.width,  0.0 / texture.height,
				-1.0 / texture.width,  0.0 / texture.height,
				-2.0 / texture.width,  0.0 / texture.height,
				-3.0 / texture.width,  0.0 / texture.height,

				 3.0 / texture.width, -1.0 / texture.height,
				 2.0 / texture.width, -1.0 / texture.height,
				 1.0 / texture.width, -1.0 / texture.height,
				 0.0 / texture.width, -1.0 / texture.height,
				-1.0 / texture.width, -1.0 / texture.height,
				-2.0 / texture.width, -1.0 / texture.height,
				-3.0 / texture.width, -1.0 / texture.height,

				 3.0 / texture.width, -2.0 / texture.height,
				 2.0 / texture.width, -2.0 / texture.height,
				 1.0 / texture.width, -2.0 / texture.height,
				 0.0 / texture.width, -2.0 / texture.height,
				-1.0 / texture.width, -2.0 / texture.height,
				-2.0 / texture.width, -2.0 / texture.height,
				-3.0 / texture.width, -2.0 / texture.height,

				 3.0 / texture.width, -3.0 / texture.height,
				 2.0 / texture.width, -3.0 / texture.height,
				 1.0 / texture.width, -3.0 / texture.height,
				 0.0 / texture.width, -3.0 / texture.height,
				-1.0 / texture.width, -3.0 / texture.height,
				-2.0 / texture.width, -3.0 / texture.height,
				-3.0 / texture.width, -3.0 / texture.height,
			};
			printf("setting uniform... (offset)\n");
			GLuint offsetLoc = glGetUniformLocation(sh_main.program, "Offset");
			glUniform2fv(offsetLoc, kernel_size, offsets);
		}
#endif
	}
	setup_convolution(sh_main.program);
}
