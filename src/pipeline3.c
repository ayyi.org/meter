/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include "agl/utils.h"
#include "agl/ext.h"
#include "agl/fbo.h"
#include "model/ayyi_model.h"
#include "meter.h"

#define DRAW_ALL
#undef DRAW_ALL

static void set_uniforms();

extern AGlShaderText circles_text;
static AGlUniformInfo uniforms[] = {
   {"height", 1, GL_FLOAT, {0,}, -1},
   {"level", 1, GL_FLOAT, {0,}, -1},
   {"peak", 1, GL_FLOAT, {0,}, -1},
   {"peak2", 1, GL_FLOAT, {0,}, -1},
   END_OF_UNIFORMS
};
static AGlShader shader = {NULL/*"circles.vert"*/, NULL/*"circles.frag"*/, 0, uniforms, set_uniforms, &circles_text};

#ifdef DRAW_ALL
extern AGlShaderText circles_many_text;
static AGlShader shader_all = {NULL/*"circles_many.vert"*/, NULL/*"circles_many.frag"*/, 0, NULL, set_uniforms, &circles_many_text};
#endif


static void
init ()
{
}


static void
set_state ()
{
#ifdef DRAW_ALL
	if(agl_get_instance()->use_shaders) agl_use_program(&shader_all);
#else
	if(agl_get_instance()->use_shaders) agl_use_program(&shader);
#endif
}


static int w = 0;
static int h = 0;

static void
set_size (PostActor* actor, int _w, int _h)
{
	// size of window. (usually) only useful for height.

	w = _w;
	h = _h;
}


static void
set_uniforms ()
{
}


#ifdef DRAW_ALL
static void
draw_all (float Xwidth, float* height, float* peak, float* max, int n)
{
	glUniform1f(glGetUniformLocation(shader_all.program, "width"), (float)w);
	glUniform1f(glGetUniformLocation(shader_all.program, "height"), (float)h);
	glUniform1fv(glGetUniformLocation(shader_all.program, "level"), 8, height);
	glUniform1fv(glGetUniformLocation(shader_all.program, "peak" ), 8, peak);
	glUniform1fv(glGetUniformLocation(shader_all.program, "peak2"), 8, max);

	glPushMatrix();
	glNormal3f(0, 0, 1);
	glBegin(GL_POLYGON);
		glVertex2f(0, 0);
		glVertex2f(w, 0);
		glVertex2f(w, h);
		glVertex2f(0, h);
	glEnd();
	glPopMatrix();
}
#endif


#ifdef DRAW_ALL
static void
draw_all_no_shaders (float Xwidth, float* height, float* peak, float* max, int n)
{
}
#endif


static inline void
set_uniform_f (AGlShader* shader, int u, float* prev)
{
	AGlUniformInfo* uniform = &shader->uniforms[u];
	if(uniform->value[0] != *prev){
		glUniform1f(uniform->location, uniform->value[0]);
		*prev = uniform->value[0];
	}
}


static void
draw (PostActor* actor, float width, float height, float peak, float max)
{
	static float prev[4] = {0,};

	shader.uniforms[0].value[0] = h;
	shader.uniforms[1].value[0] = height;
	shader.uniforms[2].value[0] = peak;
	shader.uniforms[3].value[0] = max;

	for(int i=0;i<4;i++){
		set_uniform_f(&shader, i, &prev[i]);
	}

	agl_rect (- 2 * width, 0., 4 * width, h);
}


ActorPipeline*
pipeline3_new (PostActor* actor)
{
#ifdef DRAW_ALL
	bool have_shaders = agl_get_instance()->use_shaders;
#endif

	ActorPipeline* p = AGL_NEW(ActorPipeline,
		.init = init,
		.set_size = set_size,
		.set_state = set_state,
		.draw = draw,
#ifdef DRAW_ALL
		.draw_all = have_shaders ? draw_all : draw_all_no_shaders
#endif
	);

	actor->programs = g_malloc0(sizeof(AGlShader) * 2);
#ifdef DRAW_ALL
	actor->programs[0] = &shader_all;
	dbg(2, "shader_all=%i", actor->programs[0]);
#else
	actor->programs[0] = &shader;
#endif

	return p;
}


