
// developers can set NO_STATIC_SHADER to use shaders directly without having to rerun make_shaders.
#ifndef NO_STATIC_SHADER
#define SHADER(V_FILE, L_FILE, UNIFORMS, SHADER_TEXT, SET_UNIFORMS) {NULL, NULL, 0, UNIFORMS, SET_UNIFORMS, &SHADER_TEXT}
#else
#define SHADER(V_FILE, L_FILE, UNIFORMS, SHADER_TEXT, SET_UNIFORMS) {V_FILE, L_FILE, 0, UNIFORMS, SET_UNIFORMS, NULL}
#endif

