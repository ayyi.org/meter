/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

uniform sampler2D tex2d;
const int kernel_size = 9;
//const int kernel_size = 25;

uniform vec2 offset[kernel_size];       // texture offsets
uniform vec4 kernel_value[kernel_size]; // convolution kernel
uniform vec4 scale_factor;

void main (void)
{
	int i;
	vec4 sum = vec4(0.0);

	for (i = 0; i < kernel_size; ++i) {
		sum += kernel_value[i] * vec4(texture2D(tex2d, gl_TexCoord[0].st + offset[i])); // <---- the real one
		//sum += kernel_value[i] * vec4(texture2D(tex2d, gl_TexCoord[0].st + vec2(0.0, i * 3/256)));
	}

	gl_FragColor =
		0.2 * texture2D(tex2d, gl_TexCoord[0].st) + 
		12.0 * sum * scale_factor;
}

