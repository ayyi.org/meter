/*
  This file is part of the Ayyi project. http://ayyi.org
  copyright (C) 2013 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  -----------------------------------------------------------------

  post-processing filter giving a striped tv-like effect.

*/
uniform sampler2D texture;

varying vec2 MCposition;

void main(void)
{
	gl_FragColor = 1.0 * texture2D(texture, gl_TexCoord[0].st);

	/*
	// this simple stripe is a nice effect
	if(mod(MCposition.y + 0.0, 2.0) < 1.0){
		gl_FragColor *= 0.5;
		return;
	}
	*/

	float interval = 5.0;  // stripe interval
	float d        = 2.0;  // stripe width (must not be greater than interval)
	float amount   = 0.3;  // strength of the effect

	float d_ = d / 2.0;
	float j = mod(MCposition.y, interval);
	if(j < d_){
		gl_FragColor *= (1.0 - amount) + amount * j / d_;
	}else if(j < d) {
		j -= d_;
		gl_FragColor *= (1.0 - amount) + amount * (d_ - j) / d_;
	}else{
		gl_FragColor *= (1.0 - amount);
	}
}

