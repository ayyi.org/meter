/*
  This file is part of the Ayyi project. http://ayyi.org
  copyright (C) 2012 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
uniform float height;
uniform float level;
uniform float peak;
uniform float peak2;
varying vec2 MCposition;

void main(void)
{
	if(mod(MCposition.y + 0.0, 2.0) < 1.0){
		gl_FragColor = vec4(0.0);
		return;
	}

	float mid = 2.0 * height / 3.0; // red starts at 2/3 from bottom.

	if(MCposition.y < level){
		if(MCposition.y < mid){
			float red = MCposition.y / mid;
			float grn = 1.0;
			gl_FragColor = vec4(red, grn, 0.0, 1.0);
		}else{
			float red = 1.0;
			float grn = 1.0 - (MCposition.y - mid) / (height - mid);
			//using smoothstep here doesnt help
			//gl_FragColor = vec4(red, grn, 0.0, 0.35 + 0.65 * smoothstep(level, level - 2.0, MCposition.y));
			gl_FragColor = vec4(red, grn, 0.0, 1.0);
		}
	}else{
		if(MCposition.y < peak){
			gl_FragColor = vec4(0.35, 0.35, 0.35, 0.35 * smoothstep(peak, peak - 2.0, MCposition.y));
		}else{
			if(MCposition.y < peak2 && MCposition.y > peak2 - 2.0){
				float red = MCposition.y / height;
				float grn = 1.0 - (MCposition.y) / height;
				gl_FragColor = vec4(red, grn, 0.0, 1.0);
			}else{
				gl_FragColor = vec4(0.0);
			}
		}
	}
}

