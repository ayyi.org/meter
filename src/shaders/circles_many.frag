/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2012-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
uniform float width;
uniform float height;
uniform float level[8];
uniform float peak[8];
uniform float peak2[8];
varying vec2 MCposition;

void main(void)
{
	float mid = height / 2.0;

	int n = 8;
	vec4 sum = vec4(0.0);
	int i;

	// TODO we dont have to calculate all for each pixel
	for(i=0;i<n;i++){
		vec2 pos = MCposition.xy - vec2(float(i) * (width / float(n)), mid);
		float dist_squared = dot(pos, pos) / 4.0;
		float lvl = level[i];

		//TODO we are adding colour when alpha is down...
		sum[0] = max(sum[0], lvl);
		sum[1] = max(sum[1], 0.50);
		sum[2] = max(sum[2], lvl * lvl * 0.5);
		sum[3] += max(0.0, lvl * (mid - 0.25 * dist_squared / lvl) / mid);
	}

	gl_FragColor = sum;
}

