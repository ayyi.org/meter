/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
* This file should be included from another c file.
*
*/

#if defined(USE_BLUR_H) || defined(USE_BLUR_V)
typedef struct {
	AGlShader      shader;
	struct {
		float      texture_size;
		float      gain;
		float      amount;
	}              uniform;
} BlurShader;

static void horizontal_set_uniforms();

extern AGlShaderText m_horizontal_text;
static AGlUniformInfo h_uniforms[] = {
   {"tex2d", 1, GL_INT, {1, 0, 0, 0}, -1}, // GL_TEXTURE1
   END_OF_UNIFORMS
};
static BlurShader sh_blur_h = {SHADER("horizontal.vert", "horizontal.frag", h_uniforms, m_horizontal_text, horizontal_set_uniforms)};

static void
horizontal_set_uniforms()
{
	glUniform1f(glGetUniformLocation(((AGlShader*)&sh_blur_h)->program, "texture_size"), sh_blur_h.uniform.texture_size);
	glUniform1f(glGetUniformLocation(((AGlShader*)&sh_blur_h)->program, "gain"), sh_blur_h.uniform.gain);
	glUniform1f(glGetUniformLocation(((AGlShader*)&sh_blur_h)->program, "amount"), sh_blur_h.uniform.amount);
}

#endif

#ifdef USE_BLUR_V
static void vertical_set_uniforms();

extern AGlShaderText m_vertical_text;
static AGlUniformInfo v_uniforms[] = {
   {"RTBlurH", 1, GL_INT, {1, 0, 0, 0}, -1}, // GL_TEXTURE1
   END_OF_UNIFORMS
};
static BlurShader sh_blur_v = {SHADER(NULL, NULL, v_uniforms, m_vertical_text, vertical_set_uniforms)};

static void
vertical_set_uniforms()
{
	glUniform1f(glGetUniformLocation(((AGlShader*)&sh_blur_v)->program, "texture_size"), sh_blur_v.uniform.texture_size);
	glUniform1f(glGetUniformLocation(((AGlShader*)&sh_blur_v)->program, "gain"), sh_blur_v.uniform.gain);
	glUniform1f(glGetUniformLocation(((AGlShader*)&sh_blur_v)->program, "amount"), sh_blur_v.uniform.amount);
}
#endif

