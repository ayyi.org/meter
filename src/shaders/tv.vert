varying vec2 MCposition;

void main() 
{
   gl_TexCoord[0] = gl_MultiTexCoord0;
   MCposition = gl_Vertex.xy;
   gl_Position = ftransform();
}
