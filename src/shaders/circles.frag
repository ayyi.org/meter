/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2012-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
uniform float height;
uniform float level;
uniform float peak;
uniform float peak2;
varying vec2 MCposition;

void main (void)
{
	if(mod(MCposition.y + 0.0, 2.0) < 1.0){
		gl_FragColor = vec4(0.0);
		return;
	}

	float mid = 1.0 * height / 2.0;

	float lvl_norm = level / height;

	vec2 pos = MCposition.xy - vec2(20.0, mid);
	float dist_squared = dot(pos, pos) / 4.0;
	gl_FragColor = vec4(
		lvl_norm * 1.0,
		0.50,
		lvl_norm * lvl_norm * 0.5,
		max(0.0, lvl_norm * (mid - 0.25 * dist_squared / lvl_norm) / mid)
	);

	/*
	if(MCposition.y < level){
		if(MCposition.y < mid){
			float red = MCposition.y / mid;
			float grn = 1.0;
			gl_FragColor = vec4(red, grn, 0.0, 1.0);
		}else{
			float red = 1.0;
			float grn = 1.0 - (MCposition.y - mid) / (height - mid);
			gl_FragColor = vec4(red, grn, 0.0, 1.0);
		}
	}else{
		if(MCposition.y < peak){
			gl_FragColor = vec4(0.35, 0.35, 0.35, 0.35 * smoothstep(peak, peak - 2.0, MCposition.y));
		}else{
			if(MCposition.y < peak2 && MCposition.y > peak2 - 2.0){
				float red = MCposition.y / height;
				float grn = 1.0 - (MCposition.y) / height;
				gl_FragColor = vec4(red, grn, 0.0, 1.0);
			}else{
				gl_FragColor = vec4(0.0);
			}
		}
	}
	*/
}

