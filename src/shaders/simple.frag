/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
uniform float height;
uniform float level;
uniform float peak;
uniform float peak2;
varying vec2 MCposition;

void main (void)
{
	float mid0 = 2.0 * height / 3.0; // more green than red
	float mid1 = height / 3.0;

	float y = height - MCposition.y;

	if(y < level){
		if(y < mid0){
			float red = y / mid0;
			float grn = 1.0;
			gl_FragColor = vec4(red, grn, 0.0, 1.0);
		}else{
			float red = 1.0;
			float grn = 1.0 - (y - mid0) / mid1;
			gl_FragColor = vec4(red, grn, 0.0, 1.0);
		}
	}else{
		if(y < peak){
			gl_FragColor = vec4(0.35);
		}else{
			if(y < peak2 && y > peak2 - 2.0){
				float red = y / height;
				float grn = 1.0 - (y) / height;
				gl_FragColor = vec4(red, grn, 0.0, 1.0);
			}else{
				gl_FragColor = vec4(0.0);
			}
		}
	}
}

