uniform sampler2D tex2d; // the texture to blur
uniform float texture_size;
uniform float gain;
uniform float amount;
 
void main (void)
{
	float blurSize = 1.0 / texture_size; // this size results in every step being one pixel wide
	vec4 orig = texture2D(tex2d, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y));

	// blur in y (vertical)
	// take nine samples, with the distance blurSize between them

	vec4 sum = vec4(0.0);

	sum += texture2D(tex2d, vec2(gl_TexCoord[0].x - 4.0*blurSize, gl_TexCoord[0].y)) * 0.05;
	sum += texture2D(tex2d, vec2(gl_TexCoord[0].x - 3.0*blurSize, gl_TexCoord[0].y)) * 0.09;
	sum += texture2D(tex2d, vec2(gl_TexCoord[0].x - 2.0*blurSize, gl_TexCoord[0].y)) * 0.12;
	sum += texture2D(tex2d, vec2(gl_TexCoord[0].x - 1.0*blurSize, gl_TexCoord[0].y)) * 0.15;
	sum += orig                                                                      * 0.16;
	sum += texture2D(tex2d, vec2(gl_TexCoord[0].x + 1.0*blurSize, gl_TexCoord[0].y)) * 0.15;
	sum += texture2D(tex2d, vec2(gl_TexCoord[0].x + 2.0*blurSize, gl_TexCoord[0].y)) * 0.12;
	sum += texture2D(tex2d, vec2(gl_TexCoord[0].x + 3.0*blurSize, gl_TexCoord[0].y)) * 0.09;
	sum += texture2D(tex2d, vec2(gl_TexCoord[0].x + 4.0*blurSize, gl_TexCoord[0].y)) * 0.05;

	gl_FragColor = gain * (orig * (1.0 - amount) + sum * amount);
}
