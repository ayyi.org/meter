/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
uniform sampler2D RTBlurH; // this should hold the texture rendered by the horizontal blur pass
uniform float texture_size;
uniform float gain;
uniform float amount;

void main(void)
{
	float blurSize = 1.0 / texture_size; // this size results in every step being one pixel wide

	vec4 fx = vec4(0.0);

	// blur in y (vertical)
	// take nine samples, with the distance blurSize between them
	fx += texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y - 4.0*blurSize)) * 0.05;
	fx += texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y - 3.0*blurSize)) * 0.09;
	fx += texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y - 2.0*blurSize)) * 0.12;
	fx += texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y -     blurSize)) * 0.15;
	//sum += 0.50 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 0.0)));
	/*
	sum += 0.20 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 1.0 * blurSize)));
	sum += 0.20 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 2.0 * blurSize)));
	sum += 0.20 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 3.0 * blurSize)));
	sum += 0.20 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 4.0 * blurSize)));
	sum += 0.20 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 5.0 * blurSize)));
	sum += 0.20 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 6.0 * blurSize)));
	*/

	//sum += 0.60 * vec4(texture2D(RTBlurH, vec2(gl_TexCoord[0].x + gl_TexCoord[0].y -  0.0 * blurSize)));
	/*
	sum += 0.40 * vec4(texture2D(RTBlurH, vec2(gl_TexCoord[0].x + gl_TexCoord[0].y +  2.0 * blurSize)));
	sum += 0.30 * vec4(texture2D(RTBlurH, vec2(gl_TexCoord[0].x + gl_TexCoord[0].y +  4.0 * blurSize)));
	sum += 0.20 * vec4(texture2D(RTBlurH, vec2(gl_TexCoord[0].x + gl_TexCoord[0].y +  6.0 * blurSize)));
	sum += 0.12 * vec4(texture2D(RTBlurH, vec2(gl_TexCoord[0].x + gl_TexCoord[0].y +  8.0 * blurSize)));
	sum += 0.10 * vec4(texture2D(RTBlurH, vec2(gl_TexCoord[0].x + gl_TexCoord[0].y + 10.0 * blurSize)));
	sum += 0.08 * vec4(texture2D(RTBlurH, vec2(gl_TexCoord[0].x + gl_TexCoord[0].y + 12.0 * blurSize)));
	*/
	/*
	sum += 0.15 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, -35.0)));
	sum += 0.15 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, -30.0)));
	sum += 0.15 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, -25.0)));
	sum += 0.15 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, -20.0)));
	sum += 0.25 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, -15.0)));
	sum += 0.35 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, -10.0)));
	sum += 0.45 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0,  -5.0)));

	sum += 0.45 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 5.0)));
	sum += 0.35 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 10.0)));
	sum += 0.25 * vec4(texture2D(RTBlurH, gl_TexCoord[0].st + vec2(0.0, 15.0)));
	*/

	vec4 orig = texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y));
	fx += orig * 0.18;
	fx += texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y +     blurSize)) * 0.15;
	fx += texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y + 2.0*blurSize)) * 0.12;
	fx += texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y + 3.0*blurSize)) * 0.09;
	fx += texture2D(RTBlurH, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y + 4.0*blurSize)) * 0.05;

	gl_FragColor = gain * (orig * (1.0 - amount) + fx * amount);
}
