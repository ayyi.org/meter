Ayyi Meter
----------

An OpenGL meterbridge for the Ayyi music production system.

It is packaged as both a standalone application and as an embeddable
widget.

![Screenshot](screenshot.png "Ayyi Meter Screenshot")

## How does it work?

It connects to the song and mixer services to obtain the meter signals.
When embedded inside another application using xembed, it uses a peer-to-peer
dbus communication for control.

