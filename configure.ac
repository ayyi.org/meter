AC_PREREQ(2.59)
AC_INIT([Ayyi_Meter],[0.0.1],[tim@orford.org])
AC_CONFIG_SRCDIR([src/main.c])
AC_CONFIG_HEADER([config.h])
AM_INIT_AUTOMAKE
AM_PROG_AR

# LT_REVISION=1

AC_PROG_CXX
AC_C_CONST
AC_C_INLINE
AC_HEADER_STDBOOL
AC_TYPE_SIZE_T
AC_CONFIG_MACRO_DIRS([m4])

# Library building stuff
AC_PROG_RANLIB
AC_PROG_LIBTOOL

AX_SUBDIRS_CONFIGURE([lib/ayyi], [[--disable-ffmpeg]], [], [])

privdatadir='${datadir}'/ayyi_meter
AC_SUBST(privdatadir)

# Check for debugging flag
debug="yes"
AC_ARG_ENABLE(debug,
	[AS_HELP_STRING(--enable-debug, [Enable debugging (false)])],
	[debug="$enableval"])
if test "$debug" = "yes"; then
  CFLAGS="$CFLAGS -O2 -g -DDEBUG -Wall"
  CXXFLAGS="$CFLAGS"
else
  CFLAGS="$CFLAGS -DNDEBUG"
  CXXFLAGS="$CXXFLAGS -DNDEBUG"
fi

# Check for strict flag
strict="no"
AC_ARG_ENABLE(strict,
	[AS_HELP_STRING(--enable-strict, [Enable strict compiler warnings and errors (false)])],
	[strict="$enableval"])
if test "$strict" = "yes"; then
  CFLAGS="$CFLAGS -ansi -Wall -Wextra -Wno-unused-parameter -Wconversion -Winit-self"
  CXXFLAGS="$CXXFLAGS -ansi -Wall -Wextra -Wno-unused-parameter -Wconversion -Winit-self -Woverloaded-virtual -Wsign-promo"
fi

JACK_LIBS=`pkg-config jack --cflags --libs`
AC_SUBST(JACK_LIBS)

SNDFILE_LIBS=`pkg-config --cflags --libs sndfile`
AC_SUBST(SNDFILE_LIBS)

GLIB_CFLAGS=`pkg-config --cflags glib-2.0`' '`pkg-config --cflags gobject-2.0`' '`pkg-config --cflags gmodule-2.0`
AC_SUBST(GLIB_CFLAGS)
GLIB_LDFLAGS=`pkg-config --libs gmodule-2.0`
AC_SUBST(GLIB_LDFLAGS)
GTHREAD_LIBS=`pkg-config --libs gthread-2.0`
AC_SUBST(GTHREAD_LIBS)
DBUS_CFLAGS=`pkg-config --cflags dbus-glib-1`
AC_SUBST(DBUS_CFLAGS)
DBUS_LIBS=`pkg-config --libs dbus-glib-1`
AC_SUBST(DBUS_LIBS)

WAVEFORM_CFLAGS=-I`pwd`/lib/ayyi/lib/waveform' -I'`pwd`/lib/ayyi/lib/waveform/gtkglext-1.0' '`pkg-config --cflags libass`' '`pkg-config --cflags graphene-1.0`
AC_SUBST(WAVEFORM_CFLAGS)

dnl cannot use the pc file because it is not yet generated
WAVEFORM_LDFLAGS=`pwd`"/lib/ayyi/lib/waveform/libwaveformcore.la "`pwd`"/lib/ayyi/lib/waveform/libwaveformui.la "`pwd`"/lib/ayyi/lib/waveform/gtkglext-1.0/libgtkglext.la "`pkg-config --libs graphene-1.0`
AC_SUBST(WAVEFORM_LDFLAGS)

CAIRO_CFLAGS=`pkg-config --cflags cairo`
AC_SUBST(CAIRO_CFLAGS)
CAIRO_LIBS=`pkg-config --libs cairo`
AC_SUBST(CAIRO_LIBS)

dnl ---------------------- dbus -------------------------

dbus="yes"
AC_ARG_ENABLE(dbus,
[  --disable-dbus          use DAE messaging instead of dbus],[
  case "$enableval" in
    "yes")
      ;;
    "no")
      AC_MSG_WARN([dbus not found])
      dbus="no"
      ;;
    *)
      AC_MSG_ERROR([must use --enable-dbus(=yes/no) or --disable-dbus])
      ;;
  esac
])
if `test "$dbus" = "yes"`; then
    AC_CHECK_LIB(dbus-glib-1, dbus_g_bus_get, [DBUS_FOUND=yes], [echo "*** dbus not found!"])
	if test "$DBUS_FOUND" = "yes"; then
		AM_CONDITIONAL(ENABLE_DBUS, test "yes" = "yes")
		AC_DEFINE(USE_DBUS, 1, Dbus is used for messaging.)
	else
		AC_MSG_ERROR([Dbus was configured as the comms system but was not found on your machine.])
	fi;
	DBUS_CFLAGS=`pkg-config --cflags-only-I dbus-glib-1`
	DBUS_LDFLAGS=`pkg-config --libs-only-l dbus-glib-1`
	SMSYS_LDFLAGS=
else
	AM_CONDITIONAL(ENABLE_DBUS, test "yes" = "no")
	DBUS_CFLAGS=
	DBUS_LDFLAGS=
	SMSYS_LDFLAGS=-ldae
fi;
AC_SUBST(DBUS_CFLAGS)
AC_SUBST(DBUS_LDFLAGS)

dnl -----------------------------------------------------

GTK_CFLAGS=`pkg-config gtk+-2.0 --cflags`
GTK_LIBS=`pkg-config gtk+-2.0 --libs`
AC_SUBST(GTK_CFLAGS)
AC_SUBST(GTK_LIBS)

if `pkg-config gtk+-2.0 --atleast-version=2.12`; then
  HAVE_GTK_2_12="yes"
  AC_DEFINE(HAVE_GTK_2_12, 1, "we have at least gtk version 2.12")
else
  HAVE_GTK_2_12="no"
fi;

if `pkg-config gtk+-2.0 --atleast-version=2.10`; then
  HAVE_GTK_2_10="yes"
  AC_DEFINE(HAVE_GTK_2_10, 1, "we have at least gtk version 2.10")
else
  HAVE_GTK_2_10="no"
fi;

dnl -opengl----------------------------------------------

OPENGL_LDFLAGS=`pkg-config --libs-only-l gl`
AM_CONDITIONAL(ENABLE_OPENGL, test "yes" = "yes")
enable_opengl="yes"
AC_SUBST(OPENGL_CFLAGS)
AC_SUBST(OPENGL_LDFLAGS)
AC_DEFINE(USE_OPENGL, 1, Opengl features are enabled)

dnl -----------------------------------------------------

dnl have_lv2=yes
dnl PKG_CHECK_MODULES(LV2, [lv2],
dnl 	[LV2_CFLAGS=`pkg-config --cflags-only-I lv2`],
dnl 	[enable_lv2=no, have_lv2=no],
dnl 	[have_lv2=yes]
dnl )

dnl AC_ARG_ENABLE(lv2, [  --disable-lv2           disable building of lv2 plugin.])
dnl AC_SUBST(LV2_CFLAGS)

have_lv2="no"
enable_lv2=$have_lv2
AM_CONDITIONAL(ENABLE_LV2, test "$enable_lv2" != "no")

dnl -----------------------------------------------------

AS_IF([test "$enable_libass" != "no"], [
	PKG_CHECK_MODULES(LIBASS, libass, [AC_DEFINE(USE_LIBASS, 1, Use libass for text display) enable_libass="yes"],[
	enable_libass="no"])
])
AM_CONDITIONAL(ENABLE_LIBASS, test "$enable_libass" = "yes")
AC_SUBST(LIBASS_CFLAGS)
dnl the libass pc file seems to be missing one dir:
if test "$enable_libass" = "yes"; then
	LIBASS_CFLAGS=${LIBASS_CFLAGS}" -I/usr/include/ass"
	LIBASS_LDFLAGS=${LIBASS_LIBS}
	AC_SUBST(LIBASS_LDFLAGS)
fi

dnl -----------------------------------------------------

have_epoxy=yes
PKG_CHECK_MODULES(EPOXY, [epoxy],
	[EPOXY_LDFLAGS=`pkg-config --libs epoxy`],
	[enable_epoxy=no, have_epoxy=no],
	[have_epoxy=yes]
)

AC_ARG_ENABLE(epoxy, [  --disable-epoxy           disable use of epoxy GL dispatch library.])
AC_SUBST(EPOXY_LDFLAGS)
enable_epoxy=$have_epoxy
AM_CONDITIONAL(ENABLE_EPOXY, test "$enable_epoxy" != "no")
if test "$enable_epoxy" != "no"; then
	AC_DEFINE(USE_EPOXY, 1, Use Epoxy for GL extensions)
fi

dnl -----------------------------------------------------

AC_DEFINE(USE_FBO, 1, [enable support for opengl framebuffer objects for offscreen rendering])

AC_DEFINE(USE_SONG_SERVICE, 1, [enable support for Ayyi Song Service])

# At least level 2 optimisation is needed to allow GCC to
# determine that not all nested functions require trampolines.
CFLAGS="$CFLAGS -O2"

# extra CXXFLAGS that should always be used
dnl CXXFLAGS="$CXXFLAGS -pipe -fmessage-length=139 -fdiagnostics-show-location=every-line"
AM_CFLAGS="$CFLAGS -pipe  -fmessage-length=139 -fdiagnostics-show-location=every-line"

AC_CONFIG_FILES([
Makefile
lib/Makefile
lib/debug/Makefile
lib/xembed/Makefile
src/Makefile
src/nodes/Makefile
])
AC_CONFIG_FILES([src/lv2/Makefile])
AC_OUTPUT

AC_MSG_RESULT([])
AC_MSG_RESULT([Ayyi Meter:])
AC_MSG_RESULT([Building lv2:             $enable_lv2])
AC_MSG_RESULT([])

