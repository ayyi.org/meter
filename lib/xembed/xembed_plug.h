/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/.
 */

#ifndef __xembed_PLUG_H__
#define __xembed_PLUG_H__

#ifndef GSEAL
#define GSEAL(A) A
#endif

#include "xembed_socket.h"
#include <gtk/gtkwindow.h>


G_BEGIN_DECLS

#define XEMBED_TYPE_PLUG            (xembed_plug_get_type ())
#define XEMBED_PLUG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), XEMBED_TYPE_PLUG, XembedPlug))
#define XEMBED_PLUG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), XEMBED_TYPE_PLUG, XembedPlugClass))
#define XEMBED_IS_PLUG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XEMBED_TYPE_PLUG))
#define XEMBED_IS_PLUG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XEMBED_TYPE_PLUG))
#define XEMBED_PLUG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), XEMBED_TYPE_PLUG, XembedPlugClass))


typedef struct _XembedPlug        XembedPlug;
typedef struct _XembedPlugClass   XembedPlugClass;


struct _XembedPlug
{
  GtkWindow window;

  GdkWindow *GSEAL (socket_window);
  GtkWidget *GSEAL (modality_window);
  GtkWindowGroup *GSEAL (modality_group);
  GHashTable *GSEAL (grabbed_keys);

  guint GSEAL (same_app) : 1;
};

struct _XembedPlugClass
{
  GtkWindowClass parent_class;

  void (*embedded) (XembedPlug *plug);

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};


GType      xembed_plug_get_type              (void) G_GNUC_CONST;

#ifndef GDK_MULTIHEAD_SAFE
void       xembed_plug_construct             (XembedPlug*, GdkNativeWindow  socket_id);
GtkWidget* xembed_plug_new                   (GdkNativeWindow  socket_id);
#endif

void       xembed_plug_construct_for_display (XembedPlug*, GdkDisplay*, GdkNativeWindow socket_id);
GtkWidget* xembed_plug_new_for_display       (GdkDisplay*, GdkNativeWindow socket_id);

GdkNativeWindow xembed_plug_get_id           (XembedPlug*);

gboolean   xembed_plug_get_embedded          (XembedPlug*);

GdkWindow *xembed_plug_get_socket_window     (XembedPlug*);

void      _xembed_plug_add_to_socket         (XembedPlug*, XembedSocket*);
void      _xembed_plug_remove_from_socket    (XembedPlug*, XembedSocket*);

G_END_DECLS

#endif /* __xembed_plug_h__ */
