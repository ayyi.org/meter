/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __xembed_PLUG_PRIVATE_H__
#define __xembed_PLUG_PRIVATE_H__

/* In gtkplug.c: */
void _xembed_plug_send_delete_event      (GtkWidget*);
void _xembed_plug_add_all_grabbed_keys   (XembedPlug*);
void _xembed_plug_focus_first_last       (XembedPlug*, GtkDirectionType);
void _xembed_plug_handle_modality_on     (XembedPlug*);
void _xembed_plug_handle_modality_off    (XembedPlug*);

/* In backend-specific file: */

/*
 * _xembed_plug_windowing_get_id:
 *
 * @plug: a #XembedPlug
 *
 * Returns the native window system identifier for the plug's window.
 */
GdkNativeWindow _xembed_plug_windowing_get_id (XembedPlug *plug);

/*
 * _xembed_plug_windowing_realize_toplevel:
 *
 * @plug_window: a #XembedPlug's #GdkWindow
 *
 * Called from XembedPlug's realize method. Should tell the corresponding
 * socket that the plug has been realized.
 */
void _xembed_plug_windowing_realize_toplevel (XembedPlug *plug);

/*
 * _xembed_plug_windowing_map_toplevel:
 *
 * @plug: a #XembedPlug
 *
 * Called from XembedPlug's map method. Should tell the corresponding
 * #XembedSocket that the plug has been mapped.
 */
void _xembed_plug_windowing_map_toplevel (XembedPlug *plug);

/*
 * _xembed_plug_windowing_map_toplevel:
 *
 * @plug: a #XembedPlug
 *
 * Called from XembedPlug's unmap method. Should tell the corresponding
 * #XembedSocket that the plug has been unmapped.
 */
void _xembed_plug_windowing_unmap_toplevel (XembedPlug *plug);

/*
 * _xembed_plug_windowing_set_focus:
 *
 * @plug: a #XembedPlug
 *
 * Called from XembedPlug's set_focus method. Should tell the corresponding
 * #XembedSocket to request focus.
 */
void _xembed_plug_windowing_set_focus (XembedPlug *plug);

/*
 * _xembed_plug_windowing_add_grabbed_key:
 *
 * @plug: a #XembedPlug
 * @accelerator_key: a key
 * @accelerator_mods: modifiers for it
 *
 * Called from XembedPlug's keys_changed method. Should tell the
 * corresponding #XembedSocket to grab the key.
 */
void _xembed_plug_windowing_add_grabbed_key (XembedPlug         *plug,
					  guint            accelerator_key,
					  GdkModifierType  accelerator_mods);

/*
 * _xembed_plug_windowing_remove_grabbed_key:
 *
 * @plug: a #XembedPlug
 * @accelerator_key: a key
 * @accelerator_mods: modifiers for it
 *
 * Called from XembedPlug's keys_changed method. Should tell the
 * corresponding #XembedSocket to remove the key grab.
 */
void _xembed_plug_windowing_remove_grabbed_key (XembedPlug*, guint accelerator_key, GdkModifierType);

/*
 * _xembed_plug_windowing_focus_to_parent:
 *
 * @plug: a #XembedPlug
 * @direction: a direction
 *
 * Called from XembedPlug's focus method. Should tell the corresponding
 * #XembedSocket to move the focus.
 */
void _xembed_plug_windowing_focus_to_parent (XembedPlug* plug, GtkDirectionType direction);

/*
 * _xembed_plug_windowing_filter_func:
 *
 * @gdk_xevent: a windowing system native event
 * @event: a pre-allocated empty GdkEvent
 * @data: the #XembedPlug
 *
 * Event filter function installed on plug windows.
 */
GdkFilterReturn _xembed_plug_windowing_filter_func (GdkXEvent *gdk_xevent,
						 GdkEvent  *event,
						 gpointer   data);

#endif /* __xembed_PLUG_PRIVATE_H__ */
