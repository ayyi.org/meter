/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __xembed_socket_private_h__
#define __xembed_socket_private_h__

typedef struct _XembedSocketPrivate XembedSocketPrivate;

struct _XembedSocketPrivate
{
  gint resize_count;
};

/* In gtksocket.c: */
XembedSocketPrivate *_xembed_socket_get_private (XembedSocket *socket);

void _xembed_socket_add_grabbed_key        (XembedSocket*, guint keyval, GdkModifierType);
void _xembed_socket_remove_grabbed_key     (XembedSocket*, guint keyval, GdkModifierType);
void _xembed_socket_claim_focus            (XembedSocket*, gboolean send_event);
void _xembed_socket_add_window  	       (XembedSocket*, GdkNativeWindow xid, gboolean need_reparent);
void _xembed_socket_end_embedding          (XembedSocket*);

void _xembed_socket_handle_map_request     (XembedSocket*);
void _xembed_socket_unmap_notify           (XembedSocket*);
void _xembed_socket_advance_toplevel_focus (XembedSocket*, GtkDirectionType);

/* In backend-specific file: */

/*
 * _xembed_socket_windowing_get_id:
 *
 * @socket: a #XembedSocket
 *
 * Returns the native windowing system identifier for the plug's window.
 */
GdkNativeWindow _xembed_socket_windowing_get_id (XembedSocket *socket);

/*
 * _xembed_socket_windowing_realize_window:
 *
 */
void _xembed_socket_windowing_realize_window (XembedSocket *socket);

/*
 * _xembed_socket_windowing_end_embedding_toplevel:
 *
 */
void _xembed_socket_windowing_end_embedding_toplevel (XembedSocket *socket);

/*
 * _xembed_socket_windowing_size_request:
 *
 */
void _xembed_socket_windowing_size_request (XembedSocket *socket);

/*
 * _xembed_socket_windowing_send_key_event:
 *
 */
void _xembed_socket_windowing_send_key_event (XembedSocket *socket,
					   GdkEvent  *gdk_event,
					   gboolean   mask_key_presses);

/*
 * _xembed_socket_windowing_focus_change:
 *
 */
void _xembed_socket_windowing_focus_change (XembedSocket *socket,
					 gboolean   focus_in);

/*
 * _xembed_socket_windowing_update_active:
 *
 */
void _xembed_socket_windowing_update_active (XembedSocket *socket,
					  gboolean   active);

/*
 * _xembed_socket_windowing_update_modality:
 *
 */
void _xembed_socket_windowing_update_modality (XembedSocket *socket, gboolean modality);

/*
 * _xembed_socket_windowing_focus:
 *
 */
void _xembed_socket_windowing_focus (XembedSocket *socket, GtkDirectionType);

/*
 * _xembed_socket_windowing_send_configure_event:
 *
 */
void _xembed_socket_windowing_send_configure_event (XembedSocket *socket);

/*
 * _xembed_socket_windowing_select_plug_window_input:
 *
 * Asks the windowing system to send necessary events related to the
 * plug window to the socket window. Called only for out-of-process
 * embedding.
 */
void _xembed_socket_windowing_select_plug_window_input (XembedSocket *socket);

/*
 * _xembed_socket_windowing_embed_get_info:
 *
 * Gets whatever information necessary about an out-of-process plug
 * window.
 */
void _xembed_socket_windowing_embed_get_info (XembedSocket *socket);

/*
 * _xembed_socket_windowing_embed_notify:
 *
 */
void _xembed_socket_windowing_embed_notify (XembedSocket *socket);

/*
 * _xembed_socket_windowing_embed_get_focus_wrapped:
 *
 */
gboolean _xembed_socket_windowing_embed_get_focus_wrapped (void);

/*
 * _xembed_socket_windowing_embed_set_focus_wrapped:
 *
 */
void _xembed_socket_windowing_embed_set_focus_wrapped (void);

/*
 * _xembed_socket_windowing_filter_func:
 *
 */
GdkFilterReturn _xembed_socket_windowing_filter_func (GdkXEvent*, GdkEvent*, gpointer);

void _xembed_socket_notify_new_xid(XembedSocket *socket);

#endif /* __xembed_socket_private_h__ */
