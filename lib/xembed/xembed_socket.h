/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/.
 */

#if defined(GTK_DISABLE_SINGLE_INCLUDES) && !defined (__GTK_H_INSIDE__) && !defined (GTK_COMPILATION)
#error "Only <gtk/gtk.h> can be included directly."
#endif

#ifndef __xembed_socket_h__
#define __xembed_socket_h__

#ifndef GSEAL
#define GSEAL(A) A
#endif

#include <gtk/gtkcontainer.h>

G_BEGIN_DECLS

#define XEMBED_TYPE_SOCKET            (xembed_socket_get_type ())
#define XEMBED_SOCKET(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), XEMBED_TYPE_SOCKET, XembedSocket))
#define XEMBED_SOCKET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), XEMBED_TYPE_SOCKET, XembedSocketClass))
#define XEMBED_IS_SOCKET(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XEMBED_TYPE_SOCKET))
#define XEMBED_IS_SOCKET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XEMBED_TYPE_SOCKET))
#define XEMBED_SOCKET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), XEMBED_TYPE_SOCKET, XembedSocketClass))


typedef struct _XembedSocket        XembedSocket;
typedef struct _XembedSocketClass   XembedSocketClass;

struct _XembedSocket
{
  GtkContainer container;

  guint16 GSEAL (request_width);
  guint16 GSEAL (request_height);
  guint16 GSEAL (current_width);
  guint16 GSEAL (current_height);

  GdkWindow *GSEAL (plug_window);
  GtkWidget *GSEAL (plug_widget);

  gshort GSEAL (xembed_version); /* -1 == not xembed */
  guint GSEAL (same_app) : 1;
  guint GSEAL (focus_in) : 1;
  guint GSEAL (have_size) : 1;
  guint GSEAL (need_map) : 1;
  guint GSEAL (is_mapped) : 1;
  guint GSEAL (active) : 1;

  GtkAccelGroup *GSEAL (accel_group);
  GtkWidget *GSEAL (toplevel);
};

struct _XembedSocketClass
{
  GtkContainerClass parent_class;

  void     (*plug_added)   (XembedSocket *socket_);
  gboolean (*plug_removed) (XembedSocket *socket_);

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};


GType           xembed_socket_get_type        () G_GNUC_CONST;
GtkWidget*      xembed_socket_new             ();

void            xembed_socket_add_id          (XembedSocket*, GdkNativeWindow);
GdkNativeWindow xembed_socket_get_id          (XembedSocket*);
GdkWindow*      xembed_socket_get_plug_window (XembedSocket*);

G_END_DECLS

#endif /* __xembed_socket_h__ */
